package io.opensd.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestSequentialDatabase {

    private Attribute mAge, mGender, mHeight;
    private Instance mInst0, mInst1;

    public TestSequentialDatabase() {
        SequentialAttribute height = new SequentialAttribute(0, "height");
        height.getOrAddValue("tall");
        height.getOrAddValue("regular");
        height.getOrAddValue("short");
        SequentialAttribute age = new SequentialAttribute(1, "age");
        age.getOrAddValue("old");
        age.getOrAddValue("middle age");
        age.getOrAddValue("young");
        SequentialAttribute gender = new SequentialAttribute(2, "gender");
        gender.getOrAddValue("male");
        gender.getOrAddValue("female");
        mHeight = height;
        mAge = age;
        mGender = gender;
        mInst0 = new SequentialInstance(
                new Attribute[] { mHeight, mAge, mGender },
                new int[] { 2, 1, 1 }
        );
        mInst1 = new SequentialInstance(
                new Attribute[] { mHeight, mAge, mGender},
                new int[] { 0, 0, 0 }
        );
    }

    @Test
    public void testConstructor() {
        Database data = new SequentialDatabase(
                new Instance[] { mInst0, mInst1 },
                new Attribute[] { mHeight, mAge, mGender }
        );
        assertEquals(2, data.getNumInstances());
        assertEquals(3, data.getNumAttributes());
        Attribute attr0 = data.getAttributeAt(0);
        assertNotNull(attr0);
        assertEquals(3, data.getNumDistinctValues(0));
        assertEquals(3, attr0.getNumDistinctValues());
        Attribute attr1 = data.getAttributeAt(1);
        assertNotNull(attr1);
        assertEquals(3, data.getNumDistinctValues(1));
        assertEquals(3, attr1.getNumDistinctValues());
        Attribute attr2 = data.getAttributeAt(2);
        assertNotNull(attr2);
        assertEquals(2, data.getNumDistinctValues(2));
        assertEquals(2, attr2.getNumDistinctValues());
        Instance inst0 = data.getInstanceAt(0);
        assertEquals(mInst0.getValueIndex(0), inst0.getValueIndex(0));
        assertEquals(mInst0.getValueIndex(1), inst0.getValueIndex(1));
        assertEquals(mInst0.getValueIndex(2), inst0.getValueIndex(2));
        Instance inst1 = data.getInstanceAt(1);
        assertEquals(mInst1.getValueIndex(0), inst1.getValueIndex(0));
        assertEquals(mInst1.getValueIndex(1), inst1.getValueIndex(1));
        assertEquals(mInst1.getValueIndex(2), inst1.getValueIndex(2));
    }

    @Test
    public void testDatabaseNoInstances() {
        Attribute[] attributes = new Attribute[] { mHeight, mAge, mGender };
        String msg = String.format(Messages.sDbNeedsAtLeastOneInstance,
                SequentialDatabase.class.getName());
        try {
            new SequentialDatabase(null, attributes);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(msg, ex.getMessage());
        }

        try {
            new SequentialDatabase(new Instance[0], attributes);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testDatabaseNoAttributes() {
        Instance[] instances = new Instance[] { mInst0, mInst1 };
        String msg = String.format(Messages.sDbNeedsAtLeastOneAttribute,
                SequentialDatabase.class.getName());
        try {
            new SequentialDatabase(instances, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(msg, ex.getMessage());
        }

        try {
            new SequentialDatabase(instances, new Attribute[0]);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(msg, ex.getMessage());
        }
    }
}
