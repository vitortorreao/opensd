package io.opensd.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestSequentialAttribute {

    @Test
    public void testConstructor() {
        Attribute attr = new SequentialAttribute(0, "class");
        assertEquals(0, attr.getIndex());
        assertEquals("class", attr.getName());
    }

    @Test
    public void testInvalidIndex() {
        try {
            new SequentialAttribute(-1, "class");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sInvalidAttributeIndex, -1);
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testInvalidName() {
        try {
            new SequentialAttribute(0, "");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sInvalidAttributeName, ex.getMessage());
        }

        try {
            new SequentialAttribute(0, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sInvalidAttributeName, ex.getMessage());
        }
    }

    @Test
    public void testAttributeBuild() {
        SequentialAttribute attr = new SequentialAttribute(0, "gender");
        assertEquals(0, attr.getNumDistinctValues());
        assertEquals(0, attr.getOrAddValue("male"));
        assertEquals(1, attr.getNumDistinctValues());
        assertEquals(1, attr.getOrAddValue("female"));
        assertEquals(2, attr.getNumDistinctValues());
        assertEquals(0, attr.getOrAddValue("male"));
        assertEquals(1, attr.getOrAddValue("female"));
        assertEquals("male", attr.getStringValueAt(0));
        assertEquals("female", attr.getStringValueAt(1));
        assertEquals(0, attr.getValueIndex("male"));
        assertEquals(1, attr.getValueIndex("female"));
        assertEquals(-1, attr.getValueIndex("shemale"));
    }
}
