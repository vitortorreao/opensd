package io.opensd.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestSequentialInstance {

    private Attribute mAge, mGender, mHeight;

    public TestSequentialInstance() {
        SequentialAttribute height = new SequentialAttribute(0, "height");
        height.getOrAddValue("tall");
        height.getOrAddValue("regular");
        height.getOrAddValue("short");
        SequentialAttribute age = new SequentialAttribute(1, "age");
        age.getOrAddValue("old");
        age.getOrAddValue("middle age");
        age.getOrAddValue("young");
        SequentialAttribute gender = new SequentialAttribute(2, "gender");
        gender.getOrAddValue("male");
        gender.getOrAddValue("female");
        mHeight = height;
        mAge = age;
        mGender = gender;
    }

    @Test
    public void testConstructor() {
        Instance inst = new SequentialInstance(
                new Attribute[] { mHeight, mAge, mGender },
                new int[] { 2, 1, 1 });
        assertEquals(3, inst.getNumAttributes());
        assertEquals("short", inst.getStringValue(0));
        assertEquals("short", inst.getStringValue("height"));
        assertEquals("middle age", inst.getStringValue(1));
        assertEquals("middle age", inst.getStringValue("age"));
        assertEquals("female", inst.getStringValue(2));
        assertEquals("female", inst.getStringValue("gender"));
        assertEquals(2, inst.getValueIndex(0));
        assertEquals(1, inst.getValueIndex(1));
        assertEquals(1, inst.getValueIndex(2));
        assertEquals(2, inst.getValueIndex("height"));
        assertEquals(1, inst.getValueIndex("age"));
        assertEquals(1, inst.getValueIndex("gender"));
    }

    @Test
    public void testIllegalArgumentLengths() {
        try {
            new SequentialInstance(new Attribute[] { mHeight, mAge },
                    new int[] { 2, 1, 1 });
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sInvalidInstanceArgLengths,
                    2, 3);
            assertEquals(msg, ex.getMessage());
        }

        try {
            new SequentialInstance(new Attribute[] { mHeight, mAge, mGender },
                    new int[] { 2 });
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sInvalidInstanceArgLengths,
                    3, 1);
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testIllegalValueIndexes() {
        try {
            new SequentialInstance(new Attribute[] { mHeight, mAge },
                    new int[] { 2, 3});
        } catch (IndexOutOfBoundsException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sInvalidInstanceValueIndex,
                    3, mAge.getName(), mAge.getNumDistinctValues() - 1);
            assertEquals(msg, ex.getMessage());
        }

        try {
            new SequentialInstance(new Attribute[] { mHeight, mAge },
                    new int[] { -2, 3});
        } catch (IndexOutOfBoundsException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sNegativeInstanceValueIndex,
                    mHeight.getName(), -2);
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testRepeatedAttributes() {
        try {
            new SequentialInstance(new Attribute[] { mHeight, mAge, mAge },
                    new int[] { 2, 1, 1});
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sRepeatedAttributes,
                    SequentialInstance.class.getName(), mAge.getName());
            assertEquals(msg, ex.getMessage());
        }
    }
}
