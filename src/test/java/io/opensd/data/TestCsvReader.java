package io.opensd.data;

import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.*;

public final class TestCsvReader {

    private DatabaseReader mDbReader;

    public TestCsvReader() {
        mDbReader = new CsvReader();
    }

    @Test
    public void testEmptyFile() {
        Reader reader = new StringReader("");
        try {
            mDbReader.read(reader);
            fail();
        } catch (DatabaseReadException ex) {
            assertNull(ex.getInnerException());
            assertEquals(Messages.sNoAttributesFound, ex.getMessage());
        }
    }

    @Test
    public void testOnlyHeaderFile() {
        Reader reader = new StringReader("x1,x2,y\r\n");
        try {
            mDbReader.read(reader);
            fail();
        } catch (DatabaseReadException ex) {
            assertNull(ex.getInnerException());
            assertEquals(Messages.sNoInstancesFound, ex.getMessage());
        }
    }

    @Test
    public void testSingleInstanceFile() throws DatabaseReadException {
        Reader reader = new StringReader("x1,x2,y\r\n0.1,0.3,0\r\n");
        Database db = mDbReader.read(reader);
        assertEquals(3, db.getNumAttributes());
        assertEquals(1, db.getNumInstances());

        Attribute x1 = db.getAttributeAt(0);
        assertNotNull(x1);
        assertEquals("x1", x1.getName());
        assertEquals(0, x1.getIndex());
        assertEquals(1, x1.getNumDistinctValues());
        assertEquals("0.1", x1.getStringValueAt(0));
        assertEquals(0, x1.getValueIndex("0.1"));
        assertEquals(db.getNumDistinctValues(0), x1.getNumDistinctValues());

        Attribute x2 = db.getAttributeAt(1);
        assertNotNull(x2);
        assertEquals("x2", x2.getName());
        assertEquals(1, x2.getIndex());
        assertEquals(1, x2.getNumDistinctValues());
        assertEquals("0.3", x2.getStringValueAt(0));
        assertEquals(0, x2.getValueIndex("0.3"));
        assertEquals(db.getNumDistinctValues(1), x2.getNumDistinctValues());

        Attribute y = db.getAttributeAt(2);
        assertNotNull(y);
        assertEquals("y", y.getName());
        assertEquals(2, y.getIndex());
        assertEquals(1, y.getNumDistinctValues());
        assertEquals("0", y.getStringValueAt(0));
        assertEquals(0, y.getValueIndex("0"));
        assertEquals(db.getNumDistinctValues(2), y.getNumDistinctValues());

        Instance inst = db.getInstanceAt(0);
        assertNotNull(inst);
        assertEquals(3, inst.getNumAttributes());
        assertEquals(0, inst.getValueIndex(0));
        assertEquals(0, inst.getValueIndex("x1"));
        assertEquals(0, inst.getValueIndex(1));
        assertEquals(0, inst.getValueIndex("x2"));
        assertEquals(0, inst.getValueIndex(2));
        assertEquals(0, inst.getValueIndex("y"));
        assertEquals("0.1", inst.getStringValue(0));
        assertEquals("0.3", inst.getStringValue(1));
        assertEquals("0", inst.getStringValue(2));
    }

    @Test
    public void testMultipleInstancesFile() throws DatabaseReadException {
        String text = String.format("%s\r\n%s\r\n%s\r\n%s\r\n%s",
                "x1,x2,y",
                "0.1,0.3,p\r\n",
                "0.05,0.45,p\r\n",
                "0.1,0.05,n\r\n",
                "0.60,0.015,n");
        Reader reader = new StringReader(text);
        Database db = mDbReader.read(reader);
        assertEquals(3, db.getNumAttributes());
        assertEquals(4, db.getNumInstances());

        Attribute x1 = db.getAttributeAt(0);
        assertNotNull(x1);
        assertEquals("x1", x1.getName());
        assertEquals(0, x1.getIndex());
        assertEquals(3, x1.getNumDistinctValues());
        assertEquals("0.1", x1.getStringValueAt(0));
        assertEquals(0, x1.getValueIndex("0.1"));
        assertEquals("0.05", x1.getStringValueAt(1));
        assertEquals(1, x1.getValueIndex("0.05"));
        assertEquals("0.60", x1.getStringValueAt(2));
        assertEquals(2, x1.getValueIndex("0.60"));
        assertEquals(db.getNumDistinctValues(0), x1.getNumDistinctValues());

        Attribute x2 = db.getAttributeAt(1);
        assertNotNull(x2);
        assertEquals("x2", x2.getName());
        assertEquals(1, x2.getIndex());
        assertEquals(4, x2.getNumDistinctValues());
        assertEquals("0.3", x2.getStringValueAt(0));
        assertEquals(0, x2.getValueIndex("0.3"));
        assertEquals("0.45", x2.getStringValueAt(1));
        assertEquals(1, x2.getValueIndex("0.45"));
        assertEquals("0.05", x2.getStringValueAt(2));
        assertEquals(2, x2.getValueIndex("0.05"));
        assertEquals("0.015", x2.getStringValueAt(3));
        assertEquals(3, x2.getValueIndex("0.015"));
        assertEquals(db.getNumDistinctValues(1), x2.getNumDistinctValues());

        Attribute y = db.getAttributeAt(2);
        assertNotNull(y);
        assertEquals("y", y.getName());
        assertEquals(2, y.getIndex());
        assertEquals(2, y.getNumDistinctValues());
        assertEquals("p", y.getStringValueAt(0));
        assertEquals(0, y.getValueIndex("p"));
        assertEquals("n", y.getStringValueAt(1));
        assertEquals(1, y.getValueIndex("n"));
        assertEquals(db.getNumDistinctValues(2), y.getNumDistinctValues());

        Instance inst0 = db.getInstanceAt(0);
        assertNotNull(inst0);
        assertEquals(3, inst0.getNumAttributes());
        assertEquals(0, inst0.getValueIndex(0));
        assertEquals(0, inst0.getValueIndex("x1"));
        assertEquals(0, inst0.getValueIndex(1));
        assertEquals(0, inst0.getValueIndex("x2"));
        assertEquals(0, inst0.getValueIndex(2));
        assertEquals(0, inst0.getValueIndex("y"));
        assertEquals("0.1", inst0.getStringValue(0));
        assertEquals("0.3", inst0.getStringValue(1));
        assertEquals("p", inst0.getStringValue(2));

        Instance inst1 = db.getInstanceAt(1);
        assertNotNull(inst1);
        assertEquals(3, inst1.getNumAttributes());
        assertEquals(1, inst1.getValueIndex(0));
        assertEquals(1, inst1.getValueIndex("x1"));
        assertEquals(1, inst1.getValueIndex(1));
        assertEquals(1, inst1.getValueIndex("x2"));
        assertEquals(0, inst1.getValueIndex(2));
        assertEquals(0, inst1.getValueIndex("y"));
        assertEquals("0.05", inst1.getStringValue(0));
        assertEquals("0.45", inst1.getStringValue(1));
        assertEquals("p", inst1.getStringValue(2));

        Instance inst2 = db.getInstanceAt(2);
        assertNotNull(inst2);
        assertEquals(3, inst2.getNumAttributes());
        assertEquals(0, inst2.getValueIndex(0));
        assertEquals(0, inst2.getValueIndex("x1"));
        assertEquals(2, inst2.getValueIndex(1));
        assertEquals(2, inst2.getValueIndex("x2"));
        assertEquals(1, inst2.getValueIndex(2));
        assertEquals(1, inst2.getValueIndex("y"));
        assertEquals("0.1", inst2.getStringValue(0));
        assertEquals("0.05", inst2.getStringValue(1));
        assertEquals("n", inst2.getStringValue(2));

        Instance inst3 = db.getInstanceAt(3);
        assertNotNull(inst3);
        assertEquals(3, inst3.getNumAttributes());
        assertEquals(2, inst3.getValueIndex(0));
        assertEquals(2, inst3.getValueIndex("x1"));
        assertEquals(3, inst3.getValueIndex(1));
        assertEquals(3, inst3.getValueIndex("x2"));
        assertEquals(1, inst3.getValueIndex(2));
        assertEquals(1, inst3.getValueIndex("y"));
        assertEquals("0.60", inst3.getStringValue(0));
        assertEquals("0.015", inst3.getStringValue(1));
        assertEquals("n", inst3.getStringValue(2));
    }
}
