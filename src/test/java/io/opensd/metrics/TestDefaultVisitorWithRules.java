package io.opensd.metrics;

import io.opensd.data.*;
import io.opensd.rules.Rule;
import io.opensd.rules.parser.*;
import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class TestDefaultVisitorWithRules {
    private DefaultVisitor mVisitor;
    private Instance mMale, mFemale1, mFemale2;

    public TestDefaultVisitorWithRules() throws DatabaseReadException {
        mVisitor = new DefaultVisitor();
        DatabaseReader dbReader = new CsvReader();
        String db = "height,weight,athlete,gender\n" +
                "180,92.5,true,male\n" +
                "172,72.3,false,female\n" +
                "165,80.4,true,female\n";
        Database mDatabase = dbReader.read(new StringReader(db));
        mMale = mDatabase.getInstanceAt(0);
        mFemale1 = mDatabase.getInstanceAt(1);
        mFemale2 = mDatabase.getInstanceAt(2);
    }

    @Test
    public void testComplexRule() throws LexerException, SyntaxException {
        Rule rule = createParser(
                "${height}<175 & (${athlete}=true | ${weight}<80.0) -> " +
                        "${gender} = \"female\";")
                .parseRules().get(0);
        assertFalse(mVisitor.isCoveredByAntecedent(mMale, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale2, rule));

        assertFalse(mVisitor.isCoveredByConsequent(mMale, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale2, rule));

        assertFalse(mVisitor.isFullyCovered(mMale, rule));
        assertTrue(mVisitor.isFullyCovered(mFemale1, rule));
        assertTrue(mVisitor.isFullyCovered(mFemale2, rule));

        rule = createParser(
                "${athlete}=false | (${height} < 170 & ${weight}>80.0) -> " +
                        "${gender} = \"female\";")
                .parseRules().get(0);
        assertFalse(mVisitor.isCoveredByAntecedent(mMale, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale2, rule));

        assertFalse(mVisitor.isCoveredByConsequent(mMale, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale2, rule));

        assertFalse(mVisitor.isFullyCovered(mMale, rule));
        assertTrue(mVisitor.isFullyCovered(mFemale1, rule));
        assertTrue(mVisitor.isFullyCovered(mFemale2, rule));
    }

    @Test
    public void testBooleanRules() throws LexerException, SyntaxException {
        Rule rule = createParser(
                "${height}>170 & ${athlete}=true -> ${gender} = \"male\";")
                .parseRules().get(0);
        assertTrue(mVisitor.isCoveredByAntecedent(mMale, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mMale, rule));
        assertTrue(mVisitor.isFullyCovered(mMale, rule));
        assertFalse(mVisitor.isCoveredByAntecedent(mFemale1, rule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale1, rule));
        assertFalse(mVisitor.isFullyCovered(mFemale1, rule));
        assertFalse(mVisitor.isCoveredByAntecedent(mFemale2, rule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale2, rule));
        assertFalse(mVisitor.isFullyCovered(mFemale2, rule));

        rule = createParser(
                "${height}>170 | ${athlete}=true -> ${gender} = \"male\";")
                .parseRules().get(0);
        assertTrue(mVisitor.isCoveredByAntecedent(mMale, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale2, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mMale, rule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale1, rule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale2, rule));
        assertTrue(mVisitor.isFullyCovered(mMale, rule));
        assertFalse(mVisitor.isFullyCovered(mFemale1, rule));
        assertFalse(mVisitor.isFullyCovered(mFemale2, rule));

        rule = createParser(
                "${height}<175 & !(${athlete}=true)->${gender} = \"female\";")
                .parseRules().get(0);
        assertFalse(mVisitor.isCoveredByAntecedent(mMale, rule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale1, rule));
        assertFalse(mVisitor.isCoveredByAntecedent(mFemale2, rule));
        assertFalse(mVisitor.isCoveredByConsequent(mMale, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale1, rule));
        assertTrue(mVisitor.isCoveredByConsequent(mFemale2, rule));
        assertFalse(mVisitor.isFullyCovered(mMale, rule));
        assertTrue(mVisitor.isFullyCovered(mFemale1, rule));
        assertFalse(mVisitor.isFullyCovered(mFemale2, rule));
    }

    @Test
    public void testFullRules() throws LexerException, SyntaxException {
        RuleParser parser = createParser(
                "${height} >= 170 -> ${gender} = \"male\";\n" +
                        "${athlete} = true -> ${gender} = \"male\";");
        List<Rule> rules = parser.parseRules();
        Rule heightRule = rules.get(0);
        Rule athleteRule = rules.get(1);

        assertTrue(mVisitor.isFullyCovered(mMale, heightRule));
        assertFalse(mVisitor.isFullyCovered(mFemale1, heightRule));
        assertFalse(mVisitor.isFullyCovered(mFemale2, heightRule));
        assertTrue(mVisitor.isFullyCovered(mMale, athleteRule));
        assertFalse(mVisitor.isFullyCovered(mFemale1, athleteRule));
        assertFalse(mVisitor.isFullyCovered(mFemale2, athleteRule));

        assertTrue(mVisitor.isCoveredByAntecedent(mMale, heightRule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale1, heightRule));
        assertFalse(mVisitor.isCoveredByAntecedent(mFemale2, heightRule));
        assertTrue(mVisitor.isCoveredByAntecedent(mMale, athleteRule));
        assertFalse(mVisitor.isCoveredByAntecedent(mFemale1, athleteRule));
        assertTrue(mVisitor.isCoveredByAntecedent(mFemale2, athleteRule));

        assertTrue(mVisitor.isCoveredByConsequent(mMale, heightRule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale1, heightRule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale2, heightRule));
        assertTrue(mVisitor.isCoveredByConsequent(mMale, athleteRule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale1, athleteRule));
        assertFalse(mVisitor.isCoveredByConsequent(mFemale2, athleteRule));
    }

    private RuleParser createParser(String rules) {
        Reader reader = new StringReader(rules);
        Lexer lexer = new TokenReader(reader);
        return new RuleParser(lexer);
    }
}
