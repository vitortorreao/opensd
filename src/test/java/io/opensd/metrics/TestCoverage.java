package io.opensd.metrics;

import io.opensd.data.CsvReader;
import io.opensd.data.Database;
import io.opensd.data.DatabaseReadException;
import io.opensd.data.DatabaseReader;
import io.opensd.rules.Rule;
import io.opensd.rules.parser.*;
import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public final class TestCoverage {

    private Coverage mCoverage;

    public TestCoverage() throws DatabaseReadException {
        DatabaseReader dbReader = new CsvReader();
        String db = "height,weight,athlete,gender\n" +
                "180,92.5,true,male\n" +
                "172,72.3,false,female\n" +
                "165,80.4,true,female\n";
        Database database = dbReader.read(new StringReader(db));
        mCoverage = new Coverage(database);
    }

    @Test
    public void testConstructorError() {
        try {
            new Coverage(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullDatabaseError, ex.getMessage());
        }
    }

    @Test
    public void testCalculate() throws LexerException, SyntaxException {
        Rule rule = createParser(
                "${height}<175 & (${athlete}=true | ${weight}<80.0) -> " +
                        "${gender} = \"female\";")
                .parseRules().get(0);
        assertEquals(2 / (double) 3, mCoverage.calculate(rule), 0.0001);
        rule = createParser(
                "${height}<175 & !(${athlete}=true)->${gender} = \"female\";")
                .parseRules().get(0);
        assertEquals(1 / (double) 3, mCoverage.calculate(rule), 0.0001);
        rule = createParser(
                "${weight}>80.0 | ${athlete}=false -> ${gender} = \"female\";")
                .parseRules().get(0);
        assertEquals(1d, mCoverage.calculate(rule), 0.0001);
        rule = createParser(
                "${weight}>80.0 & ${athlete}=false -> ${gender} = \"male\";")
                .parseRules().get(0);
        assertEquals(0d, mCoverage.calculate(rule), 0.0001);
    }

    private RuleParser createParser(String rules) {
        Reader reader = new StringReader(rules);
        Lexer lexer = new TokenReader(reader);
        return new RuleParser(lexer);
    }
}
