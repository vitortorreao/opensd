package io.opensd.metrics;

import io.opensd.data.*;
import io.opensd.rules.*;
import org.junit.Test;

import java.io.StringReader;

import static org.junit.Assert.*;

public final class TestDefaultVisitor {

    private Database mDatabase;
    private DefaultVisitor mVisitor;
    private Instance mMale, mFemale1;
    private AttributeNode mHeightNode, mWeightNode, mAthleteNode, mGenderNode;

    public TestDefaultVisitor() throws DatabaseReadException {
        mVisitor = new DefaultVisitor();
        DatabaseReader dbReader = new CsvReader();
        String db = "height,weight,athlete,gender\n" +
                    "180,92.5,true,male\n" +
                    "172,72.3,false,female\n";
        mDatabase = dbReader.read(new StringReader(db));
        mMale = mDatabase.getInstanceAt(0);
        mFemale1 = mDatabase.getInstanceAt(1);

        mHeightNode = new AttributeNode("height");
        mGenderNode = new AttributeNode("gender");
        mWeightNode = new AttributeNode("weight");
        mAthleteNode = new AttributeNode("athlete");
    }

    @Test
    public void testVisitBoolValueNode() {
        BoolValueNode trueNode = new BoolValueNode("true");
        BoolValueNode falseNode = new BoolValueNode("false");
        IntValueNode intValueNode1 = new IntValueNode("180");

        assertTrue(mVisitor.visitBoolValueNode(trueNode));
        assertFalse(mVisitor.visitBoolValueNode(falseNode));

        try {
            mVisitor.visitBoolValueNode(intValueNode1);
            fail();
        } catch (RuleVisitException ex) {
            String msg = String.format(
                    Messages.sExpectedBoolValueNode,
                    intValueNode1.getValueType().name());
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testVisitBooleanNode() {
        // true for mMale, false for mFemale1
        ComparisonNode heightCompNode = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                mHeightNode, new IntValueNode("180"));
        // true for mMale and mFemale1
        ComparisonNode weightCompNode = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                mWeightNode, new FloatValueNode("92.5"));
        BoolValueNode trueNode = new BoolValueNode("true");
        BoolValueNode falseNode = new BoolValueNode("false");

        // NOT
        BooleanNode node = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { heightCompNode });
        assertFalse(mVisitor.visitBooleanNode(mMale, node));
        assertTrue(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { weightCompNode });
        assertFalse(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { trueNode });
        assertFalse(mVisitor.visitBooleanNode(mMale, node));
        node = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { falseNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));

        // AND
        node = new BooleanNode(BooleanNode.Operation.AND,
                new RuleNode[] { heightCompNode, weightCompNode, trueNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));

        BooleanNode innerNode = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { falseNode });
        node = new BooleanNode(BooleanNode.Operation.AND,
                new RuleNode[] { heightCompNode, innerNode, weightCompNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.AND,
                new RuleNode[] { weightCompNode, innerNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertTrue(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.AND,
                new RuleNode[] { heightCompNode, innerNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));

        // OR
        node = new BooleanNode(BooleanNode.Operation.OR,
                new RuleNode[] { heightCompNode, weightCompNode, trueNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertTrue(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.OR,
                new RuleNode[] { heightCompNode, innerNode });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertTrue(mVisitor.visitBooleanNode(mFemale1, node));

        BooleanNode innerNode1 = new BooleanNode(BooleanNode.Operation.NOT,
                new RuleNode[] { weightCompNode });
        node = new BooleanNode(BooleanNode.Operation.OR,
                new RuleNode[] { falseNode, heightCompNode, innerNode1 });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));

        node = new BooleanNode(BooleanNode.Operation.OR,
                new RuleNode[] { heightCompNode, innerNode1 });
        assertTrue(mVisitor.visitBooleanNode(mMale, node));
        assertFalse(mVisitor.visitBooleanNode(mFemale1, node));
    }

    @Test
    public void testVisitComparisonNodeGreaterThanOrEqual() {
        IntValueNode intValueNode1 = new IntValueNode("180");
        FloatValueNode floatValueNode1 = new FloatValueNode("92.5");

        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                mHeightNode, intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                intValueNode1, mHeightNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                mWeightNode, floatValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                floatValueNode1, mWeightNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
    }

    @Test
    public void testVisitComparisonNodeGreaterThan() {
        IntValueNode intValueNode1 = new IntValueNode("180");
        FloatValueNode floatValueNode1 = new FloatValueNode("92.5");

        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN,
                mHeightNode, intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.GREATER_THAN,
                intValueNode1, mHeightNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.GREATER_THAN,
                mWeightNode, floatValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.GREATER_THAN,
                floatValueNode1, mWeightNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
    }

    @Test
    public void testVisitComparisonNodeLesserThanOrEqual() {
        IntValueNode intValueNode1 = new IntValueNode("180");
        FloatValueNode floatValueNode1 = new FloatValueNode("92.5");

        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                mHeightNode, intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                intValueNode1, mHeightNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                mWeightNode, floatValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                floatValueNode1, mWeightNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
    }

    @Test
    public void testVisitComparisonNodeLesserThan() {
        IntValueNode intValueNode1 = new IntValueNode("180");
        FloatValueNode floatValueNode1 = new FloatValueNode("92.5");

        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN,
                mHeightNode, intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.LESSER_THAN,
                intValueNode1, mHeightNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.LESSER_THAN,
                mWeightNode, floatValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        node = new ComparisonNode(ComparisonNode.Operation.LESSER_THAN,
                floatValueNode1, mWeightNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
    }

    @Test
    public void testVisitComparisonNodeNotEqual() {
        StringValueNode strValueNode1 = new StringValueNode("male");
        StringValueNode strValueNode2 = new StringValueNode("180");
        StringValueNode strValueNode3 = new StringValueNode("90");
        StringValueNode strValueNode4 = new StringValueNode("true");
        StringValueNode strValueNode5 = new StringValueNode("false");
        IntValueNode intValueNode1 = new IntValueNode("180");
        IntValueNode intValueNode2 = new IntValueNode("90");
        BoolValueNode boolValueNode1 = new BoolValueNode("true");
        BoolValueNode boolValueNode2 = new BoolValueNode("false");

        // Attribute + String
        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.NOT_EQUAL,
                mGenderNode, strValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode1, mGenderNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
        // Attribute + int
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                mHeightNode, intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode1, mHeightNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
        // Attribute + bool
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                mAthleteNode, boolValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode1, mAthleteNode);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        assertTrue(mVisitor.visitComparisonNode(mFemale1, node));

        // int + int
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode1, intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode2, intValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode1, intValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode2, intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        // int + string
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode1, strValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode2, intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                intValueNode1, strValueNode3);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode3, intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        // bool + bool
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode1, boolValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode2, boolValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode1, boolValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode2, boolValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        // bool + string
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode4, boolValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode1, strValueNode4);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode2, strValueNode5);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode5, boolValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode1, strValueNode5);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode5, boolValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                strValueNode4, boolValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                boolValueNode2, strValueNode4);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
    }

    @Test
    public void testVisitComparisonNodeEqual() {
        StringValueNode strValueNode1 = new StringValueNode("male");
        StringValueNode strValueNode2 = new StringValueNode("180");
        StringValueNode strValueNode3 = new StringValueNode("90");
        StringValueNode strValueNode4 = new StringValueNode("true");
        StringValueNode strValueNode5 = new StringValueNode("false");
        IntValueNode intValueNode1 = new IntValueNode("180");
        IntValueNode intValueNode2 = new IntValueNode("90");
        BoolValueNode boolValueNode1 = new BoolValueNode("true");
        BoolValueNode boolValueNode2 = new BoolValueNode("false");

        // Attribute + String
        ComparisonNode node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                mGenderNode, strValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, strValueNode1,
                mGenderNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
        // Attribute + int
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, mHeightNode,
                intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode1,
                mHeightNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
        // Attribute + bool
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, mAthleteNode,
                boolValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode1, mAthleteNode);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        assertFalse(mVisitor.visitComparisonNode(mFemale1, node));

        // int + int
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode1,
                intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode2,
                intValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode1,
                intValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode2,
                intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        // int + string
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode1,
                strValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, strValueNode2,
                intValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, intValueNode1,
                strValueNode3);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL, strValueNode3,
                intValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        // bool + bool
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode1, boolValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode2, boolValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode1, boolValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode2, boolValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        // bool + string
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                strValueNode4, boolValueNode1);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode1, strValueNode4);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode2, strValueNode5);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                strValueNode5, boolValueNode2);
        assertTrue(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode1, strValueNode5);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                strValueNode5, boolValueNode1);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                strValueNode4, boolValueNode2);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
        node = new ComparisonNode(ComparisonNode.Operation.EQUAL,
                boolValueNode2, strValueNode4);
        assertFalse(mVisitor.visitComparisonNode(mMale, node));
    }

    @Test
    public void testVisitAttributeNode() {
        Instance instance = mDatabase.getInstanceAt(0);
        AttributeNode node = new AttributeNode("height");
        DefaultVisitor.ValueCell cell = mVisitor.visitAttributeNode(
                instance, node);
        assertNotNull(cell);
        assertEquals(DefaultVisitor.CellType.STRING, cell.getType());
        assertEquals("180", cell.getStringValue());
    }

    @Test
    public void testVisitAttributeNodeInvalid() {
        Instance instance = mDatabase.getInstanceAt(0);
        AttributeNode node = new AttributeNode("x1");
        try {
            mVisitor.visitAttributeNode(instance, node);
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sAttributeNotFound, "x1");
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testVisitValueNode() {
        DefaultVisitor.ValueCell cell = mVisitor.visitValueNode(
                new BoolValueNode("true"));
        assertEquals(DefaultVisitor.CellType.BOOL, cell.getType());
        assertTrue(cell.getBoolValue());

        cell = mVisitor.visitValueNode(new BoolValueNode("false"));
        assertEquals(DefaultVisitor.CellType.BOOL, cell.getType());
        assertFalse(cell.getBoolValue());

        cell = mVisitor.visitValueNode(new StringValueNode("male"));
        assertEquals(DefaultVisitor.CellType.STRING, cell.getType());
        assertEquals("male", cell.getStringValue());

        cell = mVisitor.visitValueNode(new IntValueNode("20"));
        assertEquals(DefaultVisitor.CellType.INTEGER, cell.getType());
        assertEquals(20, cell.getIntValue());
        assertEquals(20d, cell.getFloatValue(), 0.0001);

        cell = mVisitor.visitValueNode(new FloatValueNode("20.99"));
        assertEquals(DefaultVisitor.CellType.FLOAT, cell.getType());
        assertEquals(20.99d, cell.getFloatValue(), 0.0001);
    }

    @Test
    public void testVisitEqual() {
        assertTrue(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell(60)
        ));
        assertFalse(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(20),
                new DefaultVisitor.ValueCell(60)
        ));
        assertTrue(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell("60"),
                new DefaultVisitor.ValueCell(60)
        ));
        assertFalse(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell("50")
        ));
        assertTrue(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(true),
                new DefaultVisitor.ValueCell(true)
        ));
        assertTrue(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(false),
                new DefaultVisitor.ValueCell(false)
        ));
        assertFalse(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell(true),
                new DefaultVisitor.ValueCell(false)
        ));
        assertTrue(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell("20"),
                new DefaultVisitor.ValueCell("20")
        ));
        assertFalse(mVisitor.visitEqual(
                new DefaultVisitor.ValueCell("20"),
                new DefaultVisitor.ValueCell("50")
        ));
    }

    @Test
    public void testVisitGreaterThan() {
        assertTrue(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell(60.1d),
                new DefaultVisitor.ValueCell(60.0d)
        ));
        assertFalse(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell(60.1d)
        ));
        assertTrue(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell(62.0d),
                new DefaultVisitor.ValueCell(60)
        ));
        assertTrue(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell("50")
        ));
        assertFalse(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell("60"),
                new DefaultVisitor.ValueCell(70)
        ));
        assertTrue(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell(62.9d),
                new DefaultVisitor.ValueCell("50")
        ));
        assertFalse(mVisitor.visitGreaterThan(
                new DefaultVisitor.ValueCell("60"),
                new DefaultVisitor.ValueCell(69.8d)
        ));
    }

    @Test
    public void testVisitGreaterThanExceptions() {
        try {
            mVisitor.visitGreaterThan(
                    new DefaultVisitor.ValueCell(false),
                    new DefaultVisitor.ValueCell(true)
            );
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(
                    Messages.sInvalidTypeForOperation,
                    DefaultVisitor.CellType.BOOL.name(), ">");
            assertEquals(msg, ex.getMessage());
        }

        try {
            mVisitor.visitGreaterThan(
                    new DefaultVisitor.ValueCell("60"),
                    new DefaultVisitor.ValueCell("45")
            );
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(
                    Messages.sInvalidTypeForOperation,
                    DefaultVisitor.CellType.STRING.name(), ">");
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testVisitLesserThan() {
        assertFalse(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell(60.1d),
                new DefaultVisitor.ValueCell(60.0d)
        ));
        assertTrue(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell(60.1d)
        ));
        assertFalse(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell(62.0d),
                new DefaultVisitor.ValueCell(60)
        ));
        assertFalse(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell(60),
                new DefaultVisitor.ValueCell("50")
        ));
        assertTrue(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell("60"),
                new DefaultVisitor.ValueCell(70)
        ));
        assertFalse(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell(62.9d),
                new DefaultVisitor.ValueCell("50")
        ));
        assertTrue(mVisitor.visitLesserThan(
                new DefaultVisitor.ValueCell("60"),
                new DefaultVisitor.ValueCell(69.8d)
        ));
    }

    @Test
    public void testVisitLesserThanExceptions() {
        try {
            mVisitor.visitLesserThan(
                    new DefaultVisitor.ValueCell(false),
                    new DefaultVisitor.ValueCell(true)
            );
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(
                    Messages.sInvalidTypeForOperation,
                    DefaultVisitor.CellType.BOOL.name(), "<");
            assertEquals(msg, ex.getMessage());
        }

        try {
            mVisitor.visitLesserThan(
                    new DefaultVisitor.ValueCell("60"),
                    new DefaultVisitor.ValueCell("45")
            );
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(
                    Messages.sInvalidTypeForOperation,
                    DefaultVisitor.CellType.STRING.name(), "<");
            assertEquals(msg, ex.getMessage());
        }
    }

    @Test
    public void testCalculateTypeExpSameTypes() {
        DefaultVisitor.CellType result;

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.BOOL,
                DefaultVisitor.CellType.BOOL);
        assertEquals(DefaultVisitor.CellType.BOOL, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.STRING,
                DefaultVisitor.CellType.STRING);
        assertEquals(DefaultVisitor.CellType.STRING, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.FLOAT,
                DefaultVisitor.CellType.FLOAT);
        assertEquals(DefaultVisitor.CellType.FLOAT, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.INTEGER,
                DefaultVisitor.CellType.INTEGER);
        assertEquals(DefaultVisitor.CellType.INTEGER, result);
    }

    @Test
    public void testCalculateTypeExpDiffTypes() {
        DefaultVisitor.CellType result;

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.STRING,
                DefaultVisitor.CellType.BOOL);
        assertEquals(DefaultVisitor.CellType.BOOL, result);
        result = mVisitor.calculateExpType(DefaultVisitor.CellType.BOOL,
                DefaultVisitor.CellType.STRING);
        assertEquals(DefaultVisitor.CellType.BOOL, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.STRING,
                DefaultVisitor.CellType.INTEGER);
        assertEquals(DefaultVisitor.CellType.INTEGER, result);
        result = mVisitor.calculateExpType(DefaultVisitor.CellType.INTEGER,
                DefaultVisitor.CellType.STRING);
        assertEquals(DefaultVisitor.CellType.INTEGER, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.STRING,
                DefaultVisitor.CellType.FLOAT);
        assertEquals(DefaultVisitor.CellType.FLOAT, result);
        result = mVisitor.calculateExpType(DefaultVisitor.CellType.FLOAT,
                DefaultVisitor.CellType.STRING);
        assertEquals(DefaultVisitor.CellType.FLOAT, result);

        result = mVisitor.calculateExpType(DefaultVisitor.CellType.INTEGER,
                DefaultVisitor.CellType.FLOAT);
        assertEquals(DefaultVisitor.CellType.FLOAT, result);
        result = mVisitor.calculateExpType(DefaultVisitor.CellType.FLOAT,
                DefaultVisitor.CellType.INTEGER);
        assertEquals(DefaultVisitor.CellType.FLOAT, result);
    }

    @Test
    public void testCalculateTypeExpInvalid() {
        try {
            mVisitor.calculateExpType(DefaultVisitor.CellType.FLOAT,
                    DefaultVisitor.CellType.BOOL);
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sCannotCompareCells,
                    DefaultVisitor.CellType.FLOAT,
                    DefaultVisitor.CellType.BOOL);
            assertEquals(msg, ex.getMessage());
        }
        try {
            mVisitor.calculateExpType(DefaultVisitor.CellType.BOOL,
                    DefaultVisitor.CellType.FLOAT);
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sCannotCompareCells,
                    DefaultVisitor.CellType.BOOL,
                    DefaultVisitor.CellType.FLOAT);
            assertEquals(msg, ex.getMessage());
        }

        try {
            mVisitor.calculateExpType(DefaultVisitor.CellType.INTEGER,
                    DefaultVisitor.CellType.BOOL);
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sCannotCompareCells,
                    DefaultVisitor.CellType.INTEGER,
                    DefaultVisitor.CellType.BOOL);
            assertEquals(msg, ex.getMessage());
        }
        try {
            mVisitor.calculateExpType(DefaultVisitor.CellType.BOOL,
                    DefaultVisitor.CellType.INTEGER);
            fail();
        } catch (RuleVisitException ex) {
            assertNotNull(ex.getMessage());
            String msg = String.format(Messages.sCannotCompareCells,
                    DefaultVisitor.CellType.BOOL,
                    DefaultVisitor.CellType.INTEGER);
            assertEquals(msg, ex.getMessage());
        }
    }
}
