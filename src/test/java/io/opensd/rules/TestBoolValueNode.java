package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestBoolValueNode {

    @Test
    public void testConstructor() {
        BoolValueNode node = new BoolValueNode("true");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ValueNode.Type.BOOLEAN, node.getValueType());
        assertEquals("true", node.getStringValue());
        assertEquals(true, node.getBoolValue());

        node = new BoolValueNode("false");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ValueNode.Type.BOOLEAN, node.getValueType());
        assertEquals("false", node.getStringValue());
        assertEquals(false, node.getBoolValue());
    }

    @Test
    public void testConstructorErrors() {
        try {
            new BoolValueNode(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullValue, ex.getMessage());
        }

        try {
            new BoolValueNode("");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sEmptyValue, ValueNode.Type.BOOLEAN);
            assertEquals(message, ex.getMessage());
        }

        try {
            new BoolValueNode("123");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sBoolParseError, "123");
            assertEquals(message, ex.getMessage());
        }
    }
}
