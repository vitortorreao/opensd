package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestFloatValueNode {

    @Test
    public void testConstructor() {
        FloatValueNode node = new FloatValueNode("12.13");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.FLOAT, node.getValueType());
        assertEquals("12.13", node.getStringValue());
        assertEquals(12.13d, node.getFloatValue(), 0.0001);

        node = new FloatValueNode("-12.13");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.FLOAT, node.getValueType());
        assertEquals("-12.13", node.getStringValue());
        assertEquals(-12.13d, node.getFloatValue(), 0.0001);

        node = new FloatValueNode("12");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.FLOAT, node.getValueType());
        assertEquals("12", node.getStringValue());
        assertEquals(12d, node.getFloatValue(), 0.0001);

        node = new FloatValueNode("-12");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.FLOAT, node.getValueType());
        assertEquals("-12", node.getStringValue());
        assertEquals(-12d, node.getFloatValue(), 0.0001);
    }

    @Test
    public void testConstructorInvalidString() {
        try {
            new FloatValueNode(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullValue, ex.getMessage());
        }

        try {
            new FloatValueNode("");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sEmptyValue, ValueNode.Type.FLOAT);
            assertEquals(message, ex.getMessage());
        }

        try {
            new FloatValueNode("abc");
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sFloatParseError, "abc");
            assertEquals(message, ex.getMessage());
        }
    }
}
