package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestRule {

    private BooleanNode mBool;
    private ComparisonNode mComp;
    private AttributeNode mAttr;

    public TestRule() {
        BoolValueNode child1 = new BoolValueNode("true");
        ComparisonNode child2 = new ComparisonNode(
                ComparisonNode.Operation.EQUAL,
                new AttributeNode("a"),
                new StringValueNode("abc"));
        mBool = new BooleanNode(BooleanNode.Operation.OR,
                new RuleNode[] { child1, child2 });
        mComp = new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                new AttributeNode("class"),
                new StringValueNode("male"));
        mAttr = new AttributeNode("color");
    }

    @Test
    public void testConstructor() {
        BooleanNode antecedent = mBool;
        ComparisonNode consequent = mComp;
        Rule rule = new Rule(antecedent, consequent);
        BooleanNode actualAntecedent = (BooleanNode) rule.getAntecedent();
        ComparisonNode actualConsequent = (ComparisonNode) rule.getConsequent();
        assertNotNull(actualAntecedent);
        assertNotNull(actualConsequent);
        assertEquals(antecedent.getNodeType(), actualAntecedent.getNodeType());
        assertEquals(consequent.getNodeType(), actualConsequent.getNodeType());
        assertEquals(antecedent.getOperation(),
                actualAntecedent.getOperation());
        assertEquals(consequent.getOperation(),
                actualConsequent.getOperation());
        assertEquals(antecedent.size(), actualAntecedent.size());
        assertEquals(antecedent.getChildAt(0).getNodeType(),
                actualAntecedent.getChildAt(0).getNodeType());
        assertEquals(antecedent.getChildAt(1).getNodeType(),
                actualAntecedent.getChildAt(1).getNodeType());
        assertEquals(consequent.getLeft().getNodeType(),
                actualConsequent.getLeft().getNodeType());
        assertEquals(consequent.getRight().getNodeType(),
                actualConsequent.getRight().getNodeType());
    }

    @Test
    public void testInvalidAntecedent() {
        try {
            new Rule(null, mComp);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullAntecedent, ex.getMessage());
        }

        try {
            new Rule(mAttr, mBool);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sInvalidChildNodeOutputType,
                    Rule.class.getName(), NodeOutputType.BOOLEAN.name());
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testInvalidConsequent() {
        try {
            new Rule(mBool, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullConsequent, ex.getMessage());
        }

        try {
            new Rule(mBool, mAttr);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sInvalidChildNodeOutputType,
                    Rule.class.getName(), NodeOutputType.BOOLEAN.name());
            assertEquals(message, ex.getMessage());
        }
    }
}
