package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestBooleanNode {

    @Test
    public void testConstructor() {
        BoolValueNode child1 = new BoolValueNode("true");
        BoolValueNode child2 = new BoolValueNode("false");
        BooleanNode node = new BooleanNode(BooleanNode.Operation.AND,
                new BoolValueNode[] { child1, child2 });
        assertEquals(NodeType.BOOLEAN, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(BooleanNode.Operation.AND, node.getOperation());
        assertEquals(2, node.size());
        assertEquals(child1.getNodeType(), node.getChildAt(0).getNodeType());
        assertEquals(child2.getNodeType(), node.getChildAt(1).getNodeType());

        node = new BooleanNode(BooleanNode.Operation.OR,
                new BoolValueNode[] { child1, child2 });
        assertEquals(NodeType.BOOLEAN, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(BooleanNode.Operation.OR, node.getOperation());
        assertEquals(2, node.size());
        assertEquals(child1.getNodeType(), node.getChildAt(0).getNodeType());
        assertEquals(child2.getNodeType(), node.getChildAt(1).getNodeType());

        node = new BooleanNode(BooleanNode.Operation.NOT,
                new BoolValueNode[] { child1 });
        assertEquals(NodeType.BOOLEAN, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(BooleanNode.Operation.NOT, node.getOperation());
        assertEquals(1, node.size());
        assertEquals(child1.getNodeType(), node.getChildAt(0).getNodeType());
    }

    @Test
    public void testConstructorNullChildren() {
        try {
            new BooleanNode(BooleanNode.Operation.AND, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.OR, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.NOT, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }
    }

    @Test
    public void testConstructorNoChildren() {
        try {
            new BooleanNode(BooleanNode.Operation.AND, new BoolValueNode[0]);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.OR, new BoolValueNode[0]);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.NOT, new BoolValueNode[0]);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNoChildrenError, ex.getMessage());
        }
    }

    @Test
    public void testNotEnoughChildren() {
        try {
            new BooleanNode(BooleanNode.Operation.AND,
                    new BoolValueNode[] { new BoolValueNode("true") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sLessThanTwoChildrenError,
                    BooleanNode.Operation.AND);
            assertEquals(message, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.OR,
                    new BoolValueNode[] { new BoolValueNode("true") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sLessThanTwoChildrenError,
                    BooleanNode.Operation.OR);
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testTooManyChildren() {
        try {
            new BooleanNode(BooleanNode.Operation.NOT,
                    new BoolValueNode[] { new BoolValueNode("true"),
                            new BoolValueNode("false") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sTooManyChildrenError,
                    BooleanNode.Operation.NOT, 1);
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testNullChildren() {
        String message = String.format(Messages.sNullChild,
                NodeType.BOOLEAN.name());
        try {
            new BooleanNode(BooleanNode.Operation.AND,
                    new BoolValueNode[] { null, new BoolValueNode("true") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.OR,
                    new BoolValueNode[] { new BoolValueNode("true"), null });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.NOT,
                    new BoolValueNode[] { null });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testInvalidOutputTypeChildren() {
        String message = String.format(Messages.sInvalidChildNodeOutputType,
                BooleanNode.class.getName(), NodeOutputType.BOOLEAN.name());
        try {
            new BooleanNode(BooleanNode.Operation.AND,
                    new RuleNode[] { new AttributeNode("a"),
                            new BoolValueNode("true") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.OR,
                    new RuleNode[] { new BoolValueNode("true"),
                            new AttributeNode("a") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }

        try {
            new BooleanNode(BooleanNode.Operation.NOT,
                    new RuleNode[] { new AttributeNode("a") });
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(message, ex.getMessage());
        }
    }
}
