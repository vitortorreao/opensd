package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestIntValueNode {

    @Test
    public void testConstructor() {
        IntValueNode node = new IntValueNode("123");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.INTEGER, node.getValueType());
        assertEquals("123", node.getStringValue());
        assertEquals(123, node.getIntValue());

        node = new IntValueNode("-123");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.NUMBER, node.getOutputType());
        assertEquals(ValueNode.Type.INTEGER, node.getValueType());
        assertEquals("-123", node.getStringValue());
        assertEquals(-123, node.getIntValue());
    }

    @Test
    public void testConstructorInvalidString() {
        try {
            new IntValueNode(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullValue, ex.getMessage());
        }

        try {
            new IntValueNode("");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sEmptyValue, ValueNode.Type.INTEGER.name());
            assertEquals(message, ex.getMessage());
        }

        try {
            new IntValueNode("abc");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sIntParseError, "abc");
            assertEquals(message, ex.getMessage());
        }
    }
}
