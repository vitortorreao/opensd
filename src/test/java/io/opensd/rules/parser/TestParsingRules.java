package io.opensd.rules.parser;

import io.opensd.rules.*;
import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestParsingRules {

    @Test
    public void testSimpleRealisticRule() throws LexerException, SyntaxException {
        RuleParser parser = createParser("${age} < 5 -> ${class} = \"poor\";");
        List<Rule> rules = parser.parseRules();
        assertEquals(1, rules.size());
        Rule rule = rules.get(0);

        ComparisonNode antComp = (ComparisonNode) rule.getAntecedent();
        assertEquals(ComparisonNode.Operation.LESSER_THAN, antComp.getOperation());
        AttributeNode antLeft = (AttributeNode) antComp.getLeft();
        assertEquals("age", antLeft.getName());
        IntValueNode antRight = (IntValueNode) antComp.getRight();
        assertEquals(5, antRight.getIntValue());

        ComparisonNode conComp = (ComparisonNode) rule.getConsequent();
        assertEquals(ComparisonNode.Operation.EQUAL, conComp.getOperation());
        AttributeNode conLeft = (AttributeNode) conComp.getLeft();
        assertEquals("class", conLeft.getName());
        StringValueNode conRight = (StringValueNode) conComp.getRight();
        assertEquals("poor", conRight.getStringValue());
    }

    @Test
    public void testMultipleAndRule() throws LexerException, SyntaxException {
        RuleParser parser = createParser("${height} < 180 & " +
                "${weight} <= 80.5 & 30 > ${age} -> ${status} = \"poor\" & " +
                "${gender} = \"female\";");
        testMultipleRuleRun(BooleanNode.Operation.AND, parser);

        parser = createParser("(${height} < 180 & " +
                "${weight} <= 80.5 & 30 > ${age}) -> ${status} = \"poor\" & " +
                "${gender} = \"female\";");
        testMultipleRuleRun(BooleanNode.Operation.AND, parser);

        parser = createParser("${height} < 180 & " +
                "${weight} <= 80.5 & 30 > ${age} -> (${status} = \"poor\" & " +
                "${gender} = \"female\");");
        testMultipleRuleRun(BooleanNode.Operation.AND, parser);

        parser = createParser("(${height} < 180 & " +
                "${weight} <= 80.5 & 30 > ${age}) ->" +
                "(${status} = \"poor\" & ${gender} = \"female\");");
        testMultipleRuleRun(BooleanNode.Operation.AND, parser);
    }

    @Test
    public void testMultipleOrRule() throws LexerException, SyntaxException {
        RuleParser parser = createParser("${height} < 180 | " +
                "${weight} <= 80.5 | 30 > ${age} -> ${status} = \"poor\" | " +
                "${gender} = \"female\";");
        testMultipleRuleRun(BooleanNode.Operation.OR, parser);

        parser = createParser("(${height} < 180 | " +
                "${weight} <= 80.5 | 30 > ${age}) -> ${status} = \"poor\" | " +
                "${gender} = \"female\";");
        testMultipleRuleRun(BooleanNode.Operation.OR, parser);

        parser = createParser("${height} < 180 | " +
                "${weight} <= 80.5 | 30 > ${age} -> (${status} = \"poor\" | " +
                "${gender} = \"female\");");
        testMultipleRuleRun(BooleanNode.Operation.OR, parser);

        parser = createParser("(${height} < 180 | " +
                "${weight} <= 80.5 | 30 > ${age}) ->" +
                "(${status} = \"poor\" | ${gender} = \"female\");");
        testMultipleRuleRun(BooleanNode.Operation.OR, parser);
    }

    private void testMultipleRuleRun(BooleanNode.Operation operation,
            RuleParser parser) throws  LexerException, SyntaxException {
        List<Rule> rules = parser.parseRules();
        assertEquals(1, rules.size());
        Rule rule = rules.get(0);

        BooleanNode antNode = (BooleanNode) rule.getAntecedent();
        assertEquals(operation, antNode.getOperation());
        assertEquals(3, antNode.size());
        ComparisonNode height = (ComparisonNode) antNode.getChildAt(0);
        ComparisonNode weight = (ComparisonNode) antNode.getChildAt(1);
        ComparisonNode age = (ComparisonNode) antNode.getChildAt(2);
        assertEquals(ComparisonNode.Operation.LESSER_THAN,
                height.getOperation());
        assertEquals(ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                weight.getOperation());
        assertEquals(ComparisonNode.Operation.GREATER_THAN,
                age.getOperation());
        AttributeNode heightAttr = (AttributeNode) height.getLeft();
        assertEquals("height", heightAttr.getName());
        IntValueNode heightVal = (IntValueNode) height.getRight();
        assertEquals(180, heightVal.getIntValue());
        AttributeNode weightAttr = (AttributeNode) weight.getLeft();
        assertEquals("weight", weightAttr.getName());
        FloatValueNode weightVal = (FloatValueNode) weight.getRight();
        assertEquals(80.5d, weightVal.getFloatValue(), 0.0001d);
        IntValueNode ageVal = (IntValueNode) age.getLeft();
        assertEquals(30, ageVal.getIntValue());
        AttributeNode ageAttr = (AttributeNode) age.getRight();
        assertEquals("age", ageAttr.getName());

        BooleanNode conNode = (BooleanNode) rule.getConsequent();
        assertEquals(operation, conNode.getOperation());
        assertEquals(2, conNode.size());
        ComparisonNode status = (ComparisonNode) conNode.getChildAt(0);
        ComparisonNode gender = (ComparisonNode) conNode.getChildAt(1);
        assertEquals(ComparisonNode.Operation.EQUAL, status.getOperation());
        assertEquals(ComparisonNode.Operation.EQUAL, gender.getOperation());
        AttributeNode statusAttr = (AttributeNode) status.getLeft();
        assertEquals("status", statusAttr.getName());
        StringValueNode statusVal = (StringValueNode) status.getRight();
        assertEquals("poor", statusVal.getStringValue());
        AttributeNode genderAttr = (AttributeNode) gender.getLeft();
        assertEquals("gender", genderAttr.getName());
        StringValueNode genderVal = (StringValueNode) gender.getRight();
        assertEquals("female", genderVal.getStringValue());
    }

    @Test
    public void testUnnecessaryBooleans() throws LexerException, SyntaxException {
        RuleParser parser = createParser("${tall} = true -> true <> ${short}" +
                ";${tall} = !true -> !false = ${short};");
        List<Rule> rules = parser.parseRules();
        assertEquals(2, rules.size());
        Rule rule0 = rules.get(0);
        Rule rule1 = rules.get(1);

        // Rule 0
        ComparisonNode ant0 = (ComparisonNode) rule0.getAntecedent();
        assertEquals(ComparisonNode.Operation.EQUAL, ant0.getOperation());
        AttributeNode tallAttr0 = (AttributeNode) ant0.getLeft();
        assertEquals("tall", tallAttr0.getName());
        BoolValueNode tallVal0 = (BoolValueNode) ant0.getRight();
        assertTrue(tallVal0.getBoolValue());
        ComparisonNode con0 = (ComparisonNode) rule0.getConsequent();
        assertEquals(ComparisonNode.Operation.NOT_EQUAL, con0.getOperation());
        BoolValueNode shortVal0 = (BoolValueNode) con0.getLeft();
        assertTrue(shortVal0.getBoolValue());
        AttributeNode shortAttr0 = (AttributeNode) con0.getRight();
        assertEquals("short", shortAttr0.getName());

        // Rule 1
        ComparisonNode ant1 = (ComparisonNode) rule1.getAntecedent();
        assertEquals(ComparisonNode.Operation.EQUAL, ant1.getOperation());
        AttributeNode tallAttr1 = (AttributeNode) ant1.getLeft();
        assertEquals("tall", tallAttr1.getName());
        BooleanNode tallVal1 = (BooleanNode) ant1.getRight();
        assertEquals(BooleanNode.Operation.NOT, tallVal1.getOperation());
        assertEquals(1, tallVal1.size());
        BoolValueNode tallBoolVal1 = (BoolValueNode) tallVal1.getChildAt(0);
        assertTrue(tallBoolVal1.getBoolValue());
        ComparisonNode con1 = (ComparisonNode) rule1.getConsequent();
        assertEquals(ComparisonNode.Operation.EQUAL, con1.getOperation());
        BooleanNode shortVal1 = (BooleanNode) con1.getLeft();
        assertEquals(BooleanNode.Operation.NOT, shortVal1.getOperation());
        assertEquals(1, shortVal1.size());
        BoolValueNode shorBoolVal1 = (BoolValueNode) shortVal1.getChildAt(0);
        assertFalse(shorBoolVal1.getBoolValue());
        AttributeNode shortAttr1 = (AttributeNode) con1.getRight();
        assertEquals("short", shortAttr1.getName());
    }

    @Test
    public void testParenthesis() throws LexerException, SyntaxException {
        RuleParser parser = createParser(
                "!(${age} >= 20) -> ${status} = \"middle\";");
        List<Rule> rules = parser.parseRules();
        assertEquals(1, rules.size());
        Rule rule = rules.get(0);

        BooleanNode ant = (BooleanNode) rule.getAntecedent();
        assertEquals(BooleanNode.Operation.NOT, ant.getOperation());
        assertEquals(1, ant.size());
        ComparisonNode comp = (ComparisonNode) ant.getChildAt(0);
        assertEquals(ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                comp.getOperation());
        AttributeNode compLeft = (AttributeNode) comp.getLeft();
        assertEquals("age", compLeft.getName());
        IntValueNode compRight = (IntValueNode) comp.getRight();
        assertEquals(20, compRight.getIntValue());

        ComparisonNode con = (ComparisonNode) rule.getConsequent();
        assertEquals(ComparisonNode.Operation.EQUAL, con.getOperation());
        AttributeNode statusAttr = (AttributeNode) con.getLeft();
        assertEquals("status", statusAttr.getName());
        StringValueNode statusVal = (StringValueNode) con.getRight();
        assertEquals("middle", statusVal.getStringValue());
    }

    @Test
    public void testNotAndInsideParenthesis() throws LexerException, SyntaxException {
        RuleParser parser = createParser("!(${gender}<>\"female\"&" +
                "${weight}>100)->\"cover\"=${insurance};");
        List<Rule> rules = parser.parseRules();
        assertEquals(1, rules.size());
        Rule rule = rules.get(0);

        BooleanNode not = (BooleanNode) rule.getAntecedent();
        assertEquals(BooleanNode.Operation.NOT, not.getOperation());
        assertEquals(1, not.size());
        BooleanNode and = (BooleanNode) not.getChildAt(0);
        assertEquals(BooleanNode.Operation.AND, and.getOperation());
        assertEquals(2, and.size());
        ComparisonNode gender = (ComparisonNode) and.getChildAt(0);
        assertEquals(ComparisonNode.Operation.NOT_EQUAL, gender.getOperation());
        AttributeNode genderAttr = (AttributeNode) gender.getLeft();
        assertEquals("gender", genderAttr.getName());
        StringValueNode genderVal = (StringValueNode) gender.getRight();
        assertEquals("female", genderVal.getStringValue());
        ComparisonNode weight = (ComparisonNode) and.getChildAt(1);
        assertEquals(ComparisonNode.Operation.GREATER_THAN, weight.getOperation());
        AttributeNode weightAttr = (AttributeNode) weight.getLeft();
        assertEquals("weight", weightAttr.getName());
        IntValueNode weightVal = (IntValueNode) weight.getRight();
        assertEquals(100, weightVal.getIntValue());

        ComparisonNode con = (ComparisonNode) rule.getConsequent();
        assertEquals(ComparisonNode.Operation.EQUAL, con.getOperation());
        StringValueNode insuranceVal = (StringValueNode) con.getLeft();
        assertEquals("cover", insuranceVal.getStringValue());
        AttributeNode insuranceAttr = (AttributeNode) con.getRight();
        assertEquals("insurance", insuranceAttr.getName());
    }

    @Test
    public void testOrPlusAnds() throws LexerException, SyntaxException {
        RuleParser parser = createParser("${height} < 180 & " +
                "${weight} <= 80.5 | 30 > ${age} & ${gender} = \"male\" -> " +
                "${status} = \"poor\";");
        List<Rule> rules = parser.parseRules();
        assertEquals(1, rules.size());
        Rule rule = rules.get(0);

        BooleanNode or = (BooleanNode) rule.getAntecedent();
        assertEquals(BooleanNode.Operation.OR, or.getOperation());
        assertEquals(2, or.size());
        BooleanNode height_weight = (BooleanNode) or.getChildAt(0);
        assertEquals(BooleanNode.Operation.AND, height_weight.getOperation());
        assertEquals(2, height_weight.size());
        ComparisonNode height = (ComparisonNode) height_weight.getChildAt(0);
        assertEquals(ComparisonNode.Operation.LESSER_THAN,
                height.getOperation());
        AttributeNode heightAttr = (AttributeNode) height.getLeft();
        assertEquals("height", heightAttr.getName());
        IntValueNode heightVal = (IntValueNode) height.getRight();
        assertEquals(180, heightVal.getIntValue());
        ComparisonNode weight = (ComparisonNode) height_weight.getChildAt(1);
        assertEquals(ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                weight.getOperation());
        AttributeNode weightAttr = (AttributeNode) weight.getLeft();
        assertEquals("weight", weightAttr.getName());
        FloatValueNode weightVal = (FloatValueNode) weight.getRight();
        assertEquals(80.5, weightVal.getFloatValue(), 0.0001);
        BooleanNode age_gender = (BooleanNode) or.getChildAt(1);
        assertEquals(BooleanNode.Operation.AND, age_gender.getOperation());
        assertEquals(2, age_gender.size());
        ComparisonNode age = (ComparisonNode) age_gender.getChildAt(0);
        assertEquals(ComparisonNode.Operation.GREATER_THAN,
                age.getOperation());
        IntValueNode ageVal = (IntValueNode) age.getLeft();
        assertEquals(30, ageVal.getIntValue());
        AttributeNode ageAttr = (AttributeNode) age.getRight();
        assertEquals("age", ageAttr.getName());
        ComparisonNode gender = (ComparisonNode) age_gender.getChildAt(1);
        assertEquals(ComparisonNode.Operation.EQUAL, gender.getOperation());
        AttributeNode genderAttr = (AttributeNode) gender.getLeft();
        assertEquals("gender", genderAttr.getName());
        StringValueNode genderVal = (StringValueNode) gender.getRight();
        assertEquals("male", genderVal.getStringValue());
    }

    private RuleParser createParser(String rule) {
        Reader reader = new StringReader(rule);
        Lexer lexer = new TokenReader(reader);
        return new RuleParser(lexer);
    }
}
