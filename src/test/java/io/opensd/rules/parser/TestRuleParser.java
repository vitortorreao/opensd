package io.opensd.rules.parser;

import io.opensd.rules.*;
import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import static org.junit.Assert.*;

public class TestRuleParser {

    @Test
    public void testEmptyFeed() throws LexerException, SyntaxException {
        RuleParser parser = createParser("");
        List<Rule> rules = parser.parseRules();
        assertNotNull(rules);
        assertEquals(0, rules.size());
    }

    @Test
    public void testNextRuleEmptyFeed() throws LexerException, SyntaxException {
        RuleParser parser = createParser("");
        Rule rule = parser.parseNextRule();
        assertNull(rule);
    }

    @Test
    public void testNullLexer() {
        try {
            new RuleParser(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullLexer, ex.getMessage());
        }
    }

    @Test
    public void testSimplestRule() throws LexerException, SyntaxException {
        RuleParser parser = createParser("true -> false;");
        Rule rule = parser.parseRules().get(0);
        assertNotNull(rule);

        RuleNode ant = rule.getAntecedent();
        assertNotNull(ant);
        assertEquals(NodeType.VALUE, ant.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant.getOutputType());
        BoolValueNode antBool = (BoolValueNode) ant;
        assertEquals("true", antBool.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool.getValueType());
        assertEquals(true, antBool.getBoolValue());

        RuleNode conseq = rule.getConsequent();
        assertNotNull(conseq);
        assertEquals(NodeType.VALUE, conseq.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq.getOutputType());
        BoolValueNode conseqBool = (BoolValueNode) conseq;
        assertEquals("false", conseqBool.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool.getValueType());
        assertEquals(false, conseqBool.getBoolValue());
    }

    @Test
    public void testNextRuleOneFeed() throws LexerException, SyntaxException {
        RuleParser parser = createParser("true -> false;");
        Rule rule = parser.parseNextRule();
        assertNotNull(rule);

        RuleNode ant = rule.getAntecedent();
        assertNotNull(ant);
        assertEquals(NodeType.VALUE, ant.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant.getOutputType());
        BoolValueNode antBool = (BoolValueNode) ant;
        assertEquals("true", antBool.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool.getValueType());
        assertEquals(true, antBool.getBoolValue());

        RuleNode conseq = rule.getConsequent();
        assertNotNull(conseq);
        assertEquals(NodeType.VALUE, conseq.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq.getOutputType());
        BoolValueNode conseqBool = (BoolValueNode) conseq;
        assertEquals("false", conseqBool.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool.getValueType());
        assertEquals(false, conseqBool.getBoolValue());

        rule = parser.parseNextRule();
        assertNull(rule);
    }

    @Test
    public void testSimpleRuleWithoutEnd() throws LexerException {
        RuleParser parser = createParser("true -> false");
        try {
            parser.parseRules();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing,
                    TokenType.SEMICOLON.name());
            String fullMessage = String.format(
                    Messages.sSyntaxErrorWithLineBase,
                    1, 14, message);
            assertEquals(fullMessage, ex.getMessage());
        }
    }

    @Test
    public void testNextRuleWithoutEnd() throws LexerException {
        RuleParser parser = createParser("true -> false");
        try {
            parser.parseNextRule();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing,
                    TokenType.SEMICOLON.name());
            String fullMessage = String.format(
                    Messages.sSyntaxErrorWithLineBase,
                    1, 14, message);
            assertEquals(fullMessage, ex.getMessage());
        }
    }

    @Test
    public void testSimpleRules() throws LexerException, SyntaxException {
        RuleParser parser = createParser("TRUE->FALSE;\nFALSE->TRUE;");
        List<Rule> rules = parser.parseRules();
        assertEquals(2, rules.size());

        Rule rule0 = rules.get(0);
        assertNotNull(rule0);
        RuleNode ant0 = rule0.getAntecedent();
        assertNotNull(ant0);
        assertEquals(NodeType.VALUE, ant0.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant0.getOutputType());
        BoolValueNode antBool0 = (BoolValueNode) ant0;
        assertEquals("TRUE", antBool0.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool0.getValueType());
        assertTrue(antBool0.getBoolValue());

        RuleNode conseq0 = rule0.getConsequent();
        assertNotNull(conseq0);
        assertEquals(NodeType.VALUE, conseq0.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq0.getOutputType());
        BoolValueNode conseqBool0 = (BoolValueNode) conseq0;
        assertEquals("FALSE", conseqBool0.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool0.getValueType());
        assertFalse(conseqBool0.getBoolValue());

        Rule rule1 = rules.get(1);
        assertNotNull(rule1);
        RuleNode ant1 = rule1.getAntecedent();
        assertNotNull(ant1);
        assertEquals(NodeType.VALUE, ant1.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant1.getOutputType());
        BoolValueNode antBool1 = (BoolValueNode) ant1;
        assertEquals("FALSE", antBool1.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool1.getValueType());
        assertFalse(antBool1.getBoolValue());

        RuleNode conseq1 = rule1.getConsequent();
        assertNotNull(conseq1);
        assertEquals(NodeType.VALUE, conseq1.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq1.getOutputType());
        BoolValueNode conseqBool1 = (BoolValueNode) conseq1;
        assertEquals("TRUE", conseqBool1.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool1.getValueType());
        assertTrue(conseqBool1.getBoolValue());
    }

    @Test
    public void testNextRuleMultiple() throws LexerException, SyntaxException {
        RuleParser parser = createParser("TRUE->FALSE;\nFALSE->TRUE;");

        Rule rule0 = parser.parseNextRule();
        assertNotNull(rule0);
        RuleNode ant0 = rule0.getAntecedent();
        assertNotNull(ant0);
        assertEquals(NodeType.VALUE, ant0.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant0.getOutputType());
        BoolValueNode antBool0 = (BoolValueNode) ant0;
        assertEquals("TRUE", antBool0.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool0.getValueType());
        assertTrue(antBool0.getBoolValue());

        RuleNode conseq0 = rule0.getConsequent();
        assertNotNull(conseq0);
        assertEquals(NodeType.VALUE, conseq0.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq0.getOutputType());
        BoolValueNode conseqBool0 = (BoolValueNode) conseq0;
        assertEquals("FALSE", conseqBool0.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool0.getValueType());
        assertFalse(conseqBool0.getBoolValue());

        Rule rule1 = parser.parseNextRule();
        assertNotNull(rule1);
        RuleNode ant1 = rule1.getAntecedent();
        assertNotNull(ant1);
        assertEquals(NodeType.VALUE, ant1.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, ant1.getOutputType());
        BoolValueNode antBool1 = (BoolValueNode) ant1;
        assertEquals("FALSE", antBool1.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, antBool1.getValueType());
        assertFalse(antBool1.getBoolValue());

        RuleNode conseq1 = rule1.getConsequent();
        assertNotNull(conseq1);
        assertEquals(NodeType.VALUE, conseq1.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, conseq1.getOutputType());
        BoolValueNode conseqBool1 = (BoolValueNode) conseq1;
        assertEquals("TRUE", conseqBool1.getStringValue());
        assertEquals(ValueNode.Type.BOOLEAN, conseqBool1.getValueType());
        assertTrue(conseqBool1.getBoolValue());

        Rule end = parser.parseNextRule();
        assertNull(end);
    }

    private RuleParser createParser(String rules) {
        Reader reader = new StringReader(rules);
        Lexer lexer = new TokenReader(reader);
        return new RuleParser(lexer);
    }
}
