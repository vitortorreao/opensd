package io.opensd.rules.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TestToken {

    @Test
    public void testConstructorWithValidArgs() {
        Token token = new Token(TokenType.AND, "&", 1, 1, 2);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());
    }

    @Test
    public void testConstructorWithInvalidArgs(){
        try {
            new Token(TokenType.OR, null, -1, -1, -1);
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals(Messages.sTokenInvalidArg, ex.getMessage());
        }
        try {
            new Token(TokenType.INTEGER, "", 1, 1, 1);
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals(Messages.sTokenInvalidArg, ex.getMessage());
        }
    }
}
