package io.opensd.rules.parser;

import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.*;

public class TestSingleToken {

    @Test
    public void testEmptyFeed() throws LexerException {
        Reader reader = new StringReader("");
        Lexer lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());

        reader = new StringReader("\n");
        lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());

        reader = new StringReader("\r\n");
        lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());

        reader = new StringReader("\t");
        lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());

        reader = new StringReader(" ");
        lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());

        reader = new StringReader("\n\t    \r\n\t ");
        lexer = new TokenReader(reader);
        assertNull(lexer.nextToken());
    }

    @Test
    public void testInvalidToken() {
        try {
            Reader reader = new StringReader("#");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(String.format(Messages.sUnexpectedChar + " (at 1:1).",
                    '#'),
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\ndonald");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(String.format(Messages.sUnexpectedChar + " (at 2:1).",
                    'd'),
                    ex.getMessage());
        }
    }

    @Test
    public void testNotToken() throws LexerException {
        Reader reader = new StringReader("!");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());

        reader = new StringReader(" !");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());

        reader = new StringReader("\n!");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());

        reader = new StringReader("\n\t!");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());

        reader = new StringReader("\n\t !");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());

        reader = new StringReader("\r\n\t !");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT, token.getType());
        assertEquals("!", token.getLexeme());
    }

    @Test
    public void testAndToken() throws LexerException {
        Reader reader = new StringReader("&");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());

        reader = new StringReader(" &");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());

        reader = new StringReader("\n&");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());

        reader = new StringReader("\n\t&");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());

        reader = new StringReader("\n\t &");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());

        reader = new StringReader("\r\n\t &");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.AND, token.getType());
        assertEquals("&", token.getLexeme());
    }

    @Test
    public void testOrToken() throws LexerException {
        Reader reader = new StringReader("|");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());

        reader = new StringReader(" |");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());

        reader = new StringReader("\n|");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());

        reader = new StringReader("\n\t|");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());

        reader = new StringReader("\n\t |");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());

        reader = new StringReader("\r\n\t |");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OR, token.getType());
        assertEquals("|", token.getLexeme());
    }

    @Test
    public void testEqualToken() throws LexerException {
        Reader reader = new StringReader("=");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());

        reader = new StringReader(" =");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());

        reader = new StringReader("\n=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());

        reader = new StringReader("\n\t=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());

        reader = new StringReader("\n\t =");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());

        reader = new StringReader("\r\n\t =");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.EQUAL, token.getType());
        assertEquals("=", token.getLexeme());
    }

    @Test
    public void testSemiColonToken() throws LexerException {
        Reader reader = new StringReader(";");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());

        reader = new StringReader(" ;");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());

        reader = new StringReader("\n;");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());

        reader = new StringReader("\n\t;");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());

        reader = new StringReader("\n\t ;");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());

        reader = new StringReader("\r\n\t ;");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.SEMICOLON, token.getType());
        assertEquals(";", token.getLexeme());
    }

    @Test
    public void testOpenParenthesisToken() throws LexerException {
        Reader reader = new StringReader("(");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());

        reader = new StringReader(" (");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());

        reader = new StringReader("\n(");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());

        reader = new StringReader("\n\t(");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());

        reader = new StringReader("\n\t (");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());

        reader = new StringReader("\r\n\t (");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.OPEN_PARENTHESIS, token.getType());
        assertEquals("(", token.getLexeme());
    }

    @Test
    public void testCloseParenthesisToken() throws LexerException {
        Reader reader = new StringReader(")");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());

        reader = new StringReader(" )");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());

        reader = new StringReader("\n)");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());

        reader = new StringReader("\n\t)");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());

        reader = new StringReader("\n\t )");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());

        reader = new StringReader("\r\n\t )");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.CLOSE_PARENTHESIS, token.getType());
        assertEquals(")", token.getLexeme());
    }

    @Test
    public void testPlusToken() throws LexerException {
        Reader reader = new StringReader("+");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());

        reader = new StringReader(" +");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());

        reader = new StringReader("\n+");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());

        reader = new StringReader("\n\t+");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());

        reader = new StringReader("\n\t +");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());

        reader = new StringReader("\r\n\t +");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.PLUS, token.getType());
        assertEquals("+", token.getLexeme());
    }

    @Test
    public void testDashToken() throws LexerException {
        Reader reader = new StringReader("-");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());

        reader = new StringReader(" -");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());

        reader = new StringReader("\n-");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());

        reader = new StringReader("\n\t-");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());

        reader = new StringReader("\n\t -");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());

        reader = new StringReader("\r\n\t -");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
    }

    @Test
    public void testArrowToken() throws LexerException {
        Reader reader = new StringReader("->");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());

        reader = new StringReader(" ->");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());

        reader = new StringReader("\n->");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());

        reader = new StringReader("\n\t->");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());

        reader = new StringReader("\n\t ->");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());

        reader = new StringReader("\r\n\t ->");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.ARROW, token.getType());
        assertEquals("->", token.getLexeme());
    }

    @Test
    public void testLesserThanToken() throws LexerException {
        Reader reader = new StringReader("<");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());

        reader = new StringReader(" <");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());

        reader = new StringReader("\n<");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());

        reader = new StringReader("\n\t<");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());

        reader = new StringReader("\n\t <");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());

        reader = new StringReader("\r\n\t <");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN, token.getType());
        assertEquals("<", token.getLexeme());
    }

    @Test
    public void testLesserThanOrEqualToken() throws LexerException {
        Reader reader = new StringReader("<=");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());

        reader = new StringReader(" <=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());

        reader = new StringReader("\n<=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());

        reader = new StringReader("\n\t<=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());

        reader = new StringReader("\n\t <=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());

        reader = new StringReader("\r\n\t <=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.LESSER_THAN_OR_EQUAL, token.getType());
        assertEquals("<=", token.getLexeme());
    }

    @Test
    public void testGreaterThanToken() throws LexerException {
        Reader reader = new StringReader(">");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());

        reader = new StringReader(" >");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());

        reader = new StringReader("\n>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());

        reader = new StringReader("\n\t>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());

        reader = new StringReader("\n\t >");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());

        reader = new StringReader("\r\n\t >");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN, token.getType());
        assertEquals(">", token.getLexeme());
    }

    @Test
    public void testGreaterThanOrEqualToken() throws LexerException {
        Reader reader = new StringReader(">=");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN_OR_EQUAL, token.getType());
        assertEquals(">=", token.getLexeme());

        reader = new StringReader(" >=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN_OR_EQUAL, token.getType());
        assertEquals(">=", token.getLexeme());

        reader = new StringReader("\n>=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN_OR_EQUAL, token.getType());
        assertEquals(">=", token.getLexeme());

        reader = new StringReader("\n\t>=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN_OR_EQUAL, token.getType());
        assertEquals(">=", token.getLexeme());

        reader = new StringReader("\r\n\t >=");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.GREATER_THAN_OR_EQUAL, token.getType());
        assertEquals(">=", token.getLexeme());
    }

    @Test
    public void testNotEqualToken() throws LexerException {
        Reader reader = new StringReader("<>");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader(" <>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader("\n<>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader("\n<>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader("\n\t<>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader("\n\t <>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());

        reader = new StringReader("\r\n\t <>");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.NOT_EQUAL, token.getType());
        assertEquals("<>", token.getLexeme());
    }

    @Test
    public void testTrueLiteralToken() throws LexerException {
        Reader reader = new StringReader("true");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("true", token.getLexeme());

        reader = new StringReader(" TRUE");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("TRUE", token.getLexeme());

        reader = new StringReader("\nTRUE");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("TRUE", token.getLexeme());

        reader = new StringReader("\n\ttrue");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("true", token.getLexeme());

        reader = new StringReader("\n\t TRUE");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("TRUE", token.getLexeme());

        reader = new StringReader("\r\n\t true");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.TRUE, token.getType());
        assertEquals("true", token.getLexeme());
    }

    @Test
    public void testFalseLiteralToken() throws LexerException {
        Reader reader = new StringReader("false");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("false", token.getLexeme());

        reader = new StringReader(" false");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("false", token.getLexeme());

        reader = new StringReader("\nFALSE");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("FALSE", token.getLexeme());

        reader = new StringReader("\n\tfalse");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("false", token.getLexeme());

        reader = new StringReader("\n\t FALSE");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("FALSE", token.getLexeme());

        reader = new StringReader("\r\n\t false");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FALSE, token.getType());
        assertEquals("false", token.getLexeme());
    }

    @Test
    public void testLiteralTokenError() {
        try {
            Reader reader = new StringReader("tr");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 1:3).",
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\r\n\r\nfals");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 3:5).",
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\ntr4");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sNonExpectedChar, 'u',
                    "true", '4') + " (at 2:3).";
            assertEquals(message, ex.getMessage());
        }

        try {
            Reader reader = new StringReader("fal$e");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sNonExpectedChar, 's',
                    "false", '$') + " (at 1:4).";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testInvalidNumbers() {
        try {
            Reader reader = new StringReader("123E12");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken(); // first token is integer "123"
            lexer.nextToken(); // dangling "E"
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'E');
            assertEquals(message + " (at 1:4).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01EE20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'E');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01EE-20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'E');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01eE20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'E');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01eE-20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'E');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01ee-20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'e');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01ee20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'e');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01ee+20");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'e');
            assertEquals(message + " (at 1:8).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01.020");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken(); // first token is float "123.01"
            lexer.nextToken(); // dangling dot, will throw
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, '.');
            assertEquals(message + " (at 1:7).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("123.01e+20e-10");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken(); // first token is float "123.01e+20"
            lexer.nextToken(); // dangling "e", will throw
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, 'e');
            assertEquals(message + " (at 1:11).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader(".01");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, '.');
            assertEquals(message + " (at 1:1).", ex.getMessage());
        }

        try {
            Reader reader = new StringReader("21.");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 1:4).",
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("21.01E");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 1:7).",
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("21.01E-");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 1:8).",
                    ex.getMessage());
        }

        try {
            Reader reader = new StringReader("21.01E+");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sUnexpectedFeedEnd + " (at 1:8).",
                    ex.getMessage());
        }
    }

    @Test
    public void testFloatToken() throws LexerException {
        Reader reader = new StringReader("123.10");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader(" -123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\t-123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\n-123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\n\t-123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\n\t -123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\r\n\t -123.10");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10", token.getLexeme());

        reader = new StringReader("\n123.10E02");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10E02", token.getLexeme());

        reader = new StringReader("\n\t-123.10E02");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10E02", token.getLexeme());

        reader = new StringReader("\n\t -123.10e02");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10e02", token.getLexeme());

        reader = new StringReader("\r\n\t 123.10E+06");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10E+06", token.getLexeme());

        reader = new StringReader("\n -123.10E-12");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10E-12", token.getLexeme());

        reader = new StringReader("\n\t -123.10e+12");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10e+12", token.getLexeme());

        reader = new StringReader("\r\n\t -123.10e-12");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.FLOAT, token.getType());
        assertEquals("123.10e-12", token.getLexeme());
    }

    @Test
    public void testIntegerToken() throws LexerException {
        Reader reader = new StringReader("123");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("123", token.getLexeme());

        reader = new StringReader(" -123");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("123", token.getLexeme());

        reader = new StringReader("\t-123");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("123", token.getLexeme());

        reader = new StringReader("\n\t121323");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("121323", token.getLexeme());

        reader = new StringReader("\n\t -123");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("123", token.getLexeme());

        reader = new StringReader("\r\n\t -123");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.DASH, token.getType());
        assertEquals("-", token.getLexeme());
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.INTEGER, token.getType());
        assertEquals("123", token.getLexeme());
    }

    @Test
    public void testInvalidStringTokens() {
        try {
            Reader reader = new StringReader("\"some");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = Messages.sUnexpectedFeedEnd + " (at 1:6).";
            assertEquals(message, ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\"so\\1e\"");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sInvalidCtrlSeq, '1') +
                    " (at 1:5).";
            assertEquals(message, ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\"so1e\'");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = Messages.sUnexpectedFeedEnd + " (at 1:7).";
            assertEquals(message, ex.getMessage());
        }

        try {
            Reader reader = new StringReader("\'so1e\"");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sUnexpectedChar, '\'') +
                    " (at 1:1).";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testStringLiteralToken() throws LexerException {
        Reader reader = new StringReader("\"\"");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("", token.getLexeme());

        reader = new StringReader("\"\\\"\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\"", token.getLexeme());

        reader = new StringReader(" \"\\\"\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\"", token.getLexeme());

        reader = new StringReader("\n\"\\t\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\t", token.getLexeme());

        reader = new StringReader("\n\t\"\\n\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\n", token.getLexeme());

        reader = new StringReader("\n\t \"\\\'\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\'", token.getLexeme());

        reader = new StringReader("\r\n\t \"\\r\\n\\t \"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("\r\n\t ", token.getLexeme());

        reader = new StringReader("\r\n\t \" \\f\\b \"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals(" \f\b ", token.getLexeme());

        reader = new StringReader("\r\n\t\"omega\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("omega", token.getLexeme());

        reader = new StringReader("  \"omega _ 3\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("omega _ 3", token.getLexeme());

        reader = new StringReader("  \"omega&[3]{2}(1)\\\"0\\\"\"");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.STRING, token.getType());
        assertEquals("omega&[3]{2}(1)\"0\"", token.getLexeme());
    }

    @Test
    public void testInvalidIdentifierToken() throws LexerException {
        try {
            Reader reader = new StringReader("${}");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = Messages.sEmptyIdentifiers + " (at 1:3).";
            assertEquals(message, ex.getMessage());
        }

        try {
            Reader reader = new StringReader("${a");
            Lexer lexer = new TokenReader(reader);
            lexer.nextToken();
            fail();
        } catch (LexerException ex) {
            assertNull(ex.getInnerException());
            assertNotNull(ex.getMessage());
            String message = Messages.sUnexpectedFeedEnd + " (at 1:4).";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testIdentifierToken() throws LexerException {
        Reader reader = new StringReader("${a}");
        Lexer lexer = new TokenReader(reader);
        Token token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("a", token.getLexeme());

        reader = new StringReader(" ${abc}");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("abc", token.getLexeme());

        reader = new StringReader("\n${x.101x}");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("x.101x", token.getLexeme());

        reader = new StringReader("\n\t${ba[a][1]}");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("ba[a][1]", token.getLexeme());

        reader = new StringReader("\n\t ${x.101x}");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("x.101x", token.getLexeme());

        reader = new StringReader("\r\n\t ${x.101x}");
        lexer = new TokenReader(reader);
        token = lexer.nextToken();
        assertNotNull(token);
        assertEquals(TokenType.IDENTIFIER, token.getType());
        assertEquals("x.101x", token.getLexeme());
    }
}
