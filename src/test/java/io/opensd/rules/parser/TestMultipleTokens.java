package io.opensd.rules.parser;

import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.*;

public class TestMultipleTokens {

    @Test
    public void testFullRule() throws LexerException {
        String rule = "${a} < 10 -> ${class} = \"positive\";";
        Token[] expectedTokens = new Token[] {
            new Token(TokenType.IDENTIFIER, "a", 1, 1, 5),
            new Token(TokenType.LESSER_THAN, "<", 1, 6, 7),
            new Token(TokenType.INTEGER, "10", 1, 8, 10),
            new Token(TokenType.ARROW, "->", 1, 11, 13),
            new Token(TokenType.IDENTIFIER, "class", 1, 14, 22),
            new Token(TokenType.EQUAL, "=", 1, 23, 24),
            new Token(TokenType.STRING, "positive",1, 25, 35),
            new Token(TokenType.SEMICOLON, ";", 1, 35, 36)
        };
        checkFullRule(rule, expectedTokens);

        String adjacent = "${a}<10->${class}=\"positive\";";
        expectedTokens = new Token[] {
                new Token(TokenType.IDENTIFIER, "a", 1, 1, 5),
                new Token(TokenType.LESSER_THAN, "<", 1, 5, 6),
                new Token(TokenType.INTEGER, "10", 1, 6, 8),
                new Token(TokenType.ARROW, "->", 1, 8, 10),
                new Token(TokenType.IDENTIFIER, "class", 1, 10, 18),
                new Token(TokenType.EQUAL, "=", 1, 18, 19),
                new Token(TokenType.STRING, "positive",1, 19, 29),
                new Token(TokenType.SEMICOLON, ";", 1, 29, 30)
        };
        checkFullRule(adjacent, expectedTokens);

        rule = "${x.101}>-20&${x.102}=true->${class}<>\"negative\"";
        expectedTokens = new Token[] {
                new Token(TokenType.IDENTIFIER, "x.101", 1 , 1, 9),
                new Token(TokenType.GREATER_THAN, ">", 1, 9, 10),
                new Token(TokenType.DASH, "-", 1, 10, 11),
                new Token(TokenType.INTEGER, "20", 1, 11, 13),
                new Token(TokenType.AND, "&", 1, 13, 14),
                new Token(TokenType.IDENTIFIER, "x.102", 1, 14, 22),
                new Token(TokenType.EQUAL, "=", 1, 22, 23),
                new Token(TokenType.TRUE, "true", 1, 23, 27),
                new Token(TokenType.ARROW, "->", 1, 27, 29),
                new Token(TokenType.IDENTIFIER, "class", 1, 29, 37),
                new Token(TokenType.NOT_EQUAL, "<>", 1, 37, 39),
                new Token(TokenType.STRING, "negative", 1, 39, 49),
        };
        checkFullRule(rule, expectedTokens);

        rule = "${x.101}>=-20|${x.102}=true\n->\n${class}<>\"negative\"";
        expectedTokens = new Token[] {
                new Token(TokenType.IDENTIFIER, "x.101", 1, 1, 9),
                new Token(TokenType.GREATER_THAN_OR_EQUAL, ">=", 1, 9, 11),
                new Token(TokenType.DASH, "-", 1, 11, 12),
                new Token(TokenType.INTEGER, "20", 1, 12, 14),
                new Token(TokenType.OR, "|", 1, 14, 15),
                new Token(TokenType.IDENTIFIER, "x.102", 1, 15, 23),
                new Token(TokenType.EQUAL, "=", 1, 23, 24),
                new Token(TokenType.TRUE, "true", 1, 24, 28),
                new Token(TokenType.ARROW, "->", 2, 1, 3),
                new Token(TokenType.IDENTIFIER, "class", 3, 1, 9),
                new Token(TokenType.NOT_EQUAL, "<>", 3, 9, 11),
                new Token(TokenType.STRING, "negative", 3, 11, 21),
        };
        checkFullRule(rule, expectedTokens);

        rule = "${x.101}>-20&(${x.102}=true|${x.103}<>\"m\")\n" +
                "\t-> ${class}=\"positive\"";
        expectedTokens = new Token[] {
                new Token(TokenType.IDENTIFIER, "x.101", 1, 1, 9),
                new Token(TokenType.GREATER_THAN, ">", 1, 9, 10),
                new Token(TokenType.DASH, "-", 1, 10, 11),
                new Token(TokenType.INTEGER, "20", 1, 11, 13),
                new Token(TokenType.AND, "&", 1, 13, 14),
                new Token(TokenType.OPEN_PARENTHESIS, "(", 1, 14, 15),
                new Token(TokenType.IDENTIFIER, "x.102", 1, 15, 23),
                new Token(TokenType.EQUAL, "=", 1, 23, 24),
                new Token(TokenType.TRUE, "true", 1, 24, 28),
                new Token(TokenType.OR, "|", 1, 28, 29),
                new Token(TokenType.IDENTIFIER, "x.103", 1, 29, 37),
                new Token(TokenType.NOT_EQUAL, "<>", 1, 37, 39),
                new Token(TokenType.STRING, "m", 1, 39, 42),
                new Token(TokenType.CLOSE_PARENTHESIS, ")", 1, 42, 43),
                new Token(TokenType.ARROW, "->", 2, 2, 4),
                new Token(TokenType.IDENTIFIER, "class", 2, 5, 13),
                new Token(TokenType.EQUAL, "=", 2, 13, 14),
                new Token(TokenType.STRING, "positive", 2, 14, 24),
        };
        checkFullRule(rule, expectedTokens);

        rule = "${x.101}>-20&!(${x.102}=true|${x.103}<>\"m\")\n" +
                "-> ${class}=\"positive\"";
        expectedTokens = new Token[] {
                new Token(TokenType.IDENTIFIER, "x.101", 1, 1, 9),
                new Token(TokenType.GREATER_THAN, ">", 1, 9, 10),
                new Token(TokenType.DASH, "-", 1, 10, 11),
                new Token(TokenType.INTEGER, "20", 1, 11, 13),
                new Token(TokenType.AND, "&", 1, 13, 14),
                new Token(TokenType.NOT, "!", 1, 14, 15),
                new Token(TokenType.OPEN_PARENTHESIS, "(", 1, 15, 16),
                new Token(TokenType.IDENTIFIER, "x.102", 1, 16, 24),
                new Token(TokenType.EQUAL, "=", 1, 24, 25),
                new Token(TokenType.TRUE, "true", 1, 25, 29),
                new Token(TokenType.OR, "|", 1, 29, 30),
                new Token(TokenType.IDENTIFIER, "x.103", 1, 30, 38),
                new Token(TokenType.NOT_EQUAL, "<>", 1, 38, 40),
                new Token(TokenType.STRING, "m", 1, 40, 43),
                new Token(TokenType.CLOSE_PARENTHESIS, ")", 1, 43, 44),
                new Token(TokenType.ARROW, "->", 2, 1, 3),
                new Token(TokenType.IDENTIFIER, "class", 2, 4, 12),
                new Token(TokenType.EQUAL, "=", 2, 12, 13),
                new Token(TokenType.STRING, "positive", 2, 13, 23),
        };
        checkFullRule(rule, expectedTokens);
    }

    @Test
    public void testWeirdCombinations() throws LexerException {
        String rule = "--123";
        Token[] expectedTokens = new Token[] {
                new Token(TokenType.DASH, "-", 1, 1,2),
                new Token(TokenType.DASH, "-", 1, 2,3),
                new Token(TokenType.INTEGER, "123", 1, 3, 6),
        };
        checkFullRule(rule, expectedTokens);
    }

    private void checkFullRule(String rule, Token[] expectedTokens)
            throws LexerException {
        Reader reader = new StringReader(rule);
        Lexer lexer = new TokenReader(reader);

        for (int i = 0; i < expectedTokens.length; i++) {
            Token actualToken = lexer.nextToken();
            Token expectedToken = expectedTokens[i];
            if (expectedToken != null) assertNotNull(actualToken);
            assertEquals(expectedToken.getType(), actualToken.getType());
            assertEquals(expectedToken.getLexeme(), actualToken.getLexeme());
            if (expectedToken.getLineNumber() > 0) {
                assertEquals(expectedToken.getLineNumber(),
                        actualToken.getLineNumber());
                assertEquals(expectedToken.getStartCol(),
                        actualToken.getStartCol());
                assertEquals(expectedToken.getEndCol(),
                        actualToken.getEndCol());
            }
        }

        assertNull(lexer.nextToken());
    }
}
