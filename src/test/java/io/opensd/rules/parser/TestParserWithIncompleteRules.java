package io.opensd.rules.parser;

import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestParserWithIncompleteRules {

    @Test
    public void testParseRuleErrors() throws LexerException {
        try {
            RuleParser parser = createParser("TRUE");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing,
                    TokenType.ARROW.name());
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 5, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("FALSE->TRUE");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing,
                    TokenType.SEMICOLON.name());
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 12, message);
            assertEquals(exMessage, ex.getMessage());
        }
    }

    @Test
    public void testparseFactorErrors() throws LexerException {
        List<String> typeNames = new ArrayList<>();
        for (TokenType type : RuleParser.sCompTokens) {
            typeNames.add(type.name());
        }
        String types = String.join(", ", typeNames);

        try {
            RuleParser parser = createParser("4");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokensButFoundNothing,
                    types);
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 2, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("4!");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokensButFoundNeither,
                    types, '!');
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 2, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("TRUE&");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 6, Messages.sUnexpectedFeedEnd + ".");
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    public void testParseBoolValErrors() throws LexerException {
        try {
            RuleParser parser = createParser("(TRUE");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing,
                    TokenType.CLOSE_PARENTHESIS.name());
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 6, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("(TRUE4");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokenButFoundOther,
                    TokenType.CLOSE_PARENTHESIS.name(),
                    "4");
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 6, message);
            assertEquals(exMessage, ex.getMessage());
        }
    }

    @Test
    public void testParseElementErrors() throws LexerException {
        List<String> typeNames = new ArrayList<>();
        for (TokenType type : RuleParser.sElementTokens) {
            typeNames.add(type.name());
        }
        String types = String.join(", ", typeNames);
        try {
            RuleParser parser = createParser("4<");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokensButFoundNothing,
                    types);
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 3, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("4<<");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokensButFoundNeither,
                    types, '<');
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 3, message);
            assertEquals(exMessage, ex.getMessage());
        }

        try {
            RuleParser parser = createParser("TRUE&&");
            parser.parseRules();
            fail();
        } catch (SyntaxException ex) {
            assertNotNull(ex.getMessage());
            String message = String.format(
                    Messages.sExpectedTokensButFoundNeither,
                    types, '&');
            String exMessage = String.format(Messages.sSyntaxErrorWithLineBase,
                    1, 6, message);
            assertEquals(exMessage, ex.getMessage());
        }
    }

    private RuleParser createParser(String rules) {
        Reader reader = new StringReader(rules);
        Lexer lexer = new TokenReader(reader);
        return new RuleParser(lexer);
    }
}
