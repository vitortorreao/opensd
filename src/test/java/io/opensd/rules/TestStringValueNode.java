package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestStringValueNode {
    @Test
    public void testConstructorNoErrors() {
        ValueNode node = new StringValueNode("male");
        assertEquals(NodeType.VALUE, node.getNodeType());
        assertEquals(NodeOutputType.STRING, node.getOutputType());
        assertEquals(ValueNode.Type.STRING, node.getValueType());
        assertEquals("male", node.getStringValue());
    }

    @Test
    public void testConstructorInvalidStrings() {
        try {
            new StringValueNode(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullValue, ex.getMessage());
        }
    }
}
