package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestAttributeNode {

    @Test
    public void testConstructor() {
        AttributeNode node = new AttributeNode("x.101");
        assertEquals(NodeType.ATTRIBUTE, node.getNodeType());
        assertEquals("x.101", node.getName());
        assertEquals(NodeOutputType.UNKNOWN, node.getOutputType());
    }

    @Test
    public void testConstructorErrors() {
        try {
            new AttributeNode(null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullAttribute, ex.getMessage());
        }

        try {
            new AttributeNode("");
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullAttribute, ex.getMessage());
        }
    }
}
