package io.opensd.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestComparisonNode {

    @Test
    public void testConstructor() {
        ComparisonNode node = new ComparisonNode(
                ComparisonNode.Operation.EQUAL,
                new AttributeNode("a"),
                new StringValueNode("abc"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.EQUAL, node.getOperation());
        RuleNode left = node.getLeft();
        assertEquals(NodeType.ATTRIBUTE, left.getNodeType());
        RuleNode right = node.getRight();
        assertEquals(NodeType.VALUE, right.getNodeType());

        node = new ComparisonNode(
                ComparisonNode.Operation.NOT_EQUAL,
                new IntValueNode("12"),
                new StringValueNode("abc"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.NOT_EQUAL, node.getOperation());
        left = node.getLeft();
        assertEquals(NodeType.VALUE, left.getNodeType());
        right = node.getRight();
        assertEquals(NodeType.VALUE, right.getNodeType());

        node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN,
                new FloatValueNode("12.5"),
                new AttributeNode("b"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.GREATER_THAN,
                node.getOperation());
        left = node.getLeft();
        assertEquals(NodeType.VALUE, left.getNodeType());
        right = node.getRight();
        assertEquals(NodeType.ATTRIBUTE, right.getNodeType());

        node = new ComparisonNode(
                ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                new BoolValueNode("true"),
                new AttributeNode("b"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                node.getOperation());
        left = node.getLeft();
        assertEquals(NodeType.VALUE, left.getNodeType());
        right = node.getRight();
        assertEquals(NodeType.ATTRIBUTE, right.getNodeType());

        node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN,
                new AttributeNode("b"),
                new BoolValueNode("false"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.LESSER_THAN,
                node.getOperation());
        left = node.getLeft();
        assertEquals(NodeType.ATTRIBUTE, left.getNodeType());
        right = node.getRight();
        assertEquals(NodeType.VALUE, right.getNodeType());

        node = new ComparisonNode(
                ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                new AttributeNode("b"),
                new AttributeNode("a"));
        assertEquals(NodeType.COMPARISON, node.getNodeType());
        assertEquals(NodeOutputType.BOOLEAN, node.getOutputType());
        assertEquals(ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                node.getOperation());
        left = node.getLeft();
        assertEquals(NodeType.ATTRIBUTE, left.getNodeType());
        right = node.getRight();
        assertEquals(NodeType.ATTRIBUTE, right.getNodeType());
    }

    @Test
    public void testNullChild() {
        try {
            new ComparisonNode(ComparisonNode.Operation.EQUAL,
                    new AttributeNode("a"), null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }

        try {
            new ComparisonNode(ComparisonNode.Operation.NOT_EQUAL,
                    null, new BoolValueNode("true"));
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }

        try {
            new ComparisonNode(ComparisonNode.Operation.GREATER_THAN,
                    null, null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }

        try {
            new ComparisonNode(ComparisonNode.Operation.GREATER_THAN_OR_EQUAL,
                    new StringValueNode("abc"), null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }

        try {
            new ComparisonNode(ComparisonNode.Operation.LESSER_THAN,
                    null, new FloatValueNode("12.5"));
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }

        try {
            new ComparisonNode(ComparisonNode.Operation.LESSER_THAN_OR_EQUAL,
                    new IntValueNode("123"), null);
            fail();
        } catch (IllegalArgumentException ex) {
            assertNotNull(ex.getMessage());
            assertEquals(Messages.sNullChild, ex.getMessage());
        }
    }
}
