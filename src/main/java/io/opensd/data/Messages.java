package io.opensd.data;

final class Messages {

    private Messages() {}

    static final String sInvalidAttributeIndex;
    static final String sInvalidAttributeName;
    static final String sInvalidInstanceArgLengths;
    static final String sInvalidInstanceValueIndex;
    static final String sNegativeInstanceValueIndex;
    static final String sRepeatedAttributes;
    static final String sDbNeedsAtLeastOneInstance;
    static final String sDbNeedsAtLeastOneAttribute;
    static final String sNoAttributesFound;
    static final String sNoInstancesFound;
    static final String sDbReadExceptInnerMsg;
    static final String sAttributeNameNotFound;

    static {
        sInvalidAttributeIndex = "An attribute's index cannot be negative. " +
                "Provided value = %d.";
        sInvalidAttributeName = "Attribute names cannot be null or empty";
        sInvalidInstanceArgLengths = "Both arguments must have same length. " +
                "%d != %d.";
        sInvalidInstanceValueIndex = "Index %d for Attribute %s is invalid. " +
                "Max is %d.";
        sNegativeInstanceValueIndex = "Cannot assign a negative value " +
                "index. Provided value index for Attribute %s: %d.";
        sRepeatedAttributes = "Cannot create a %s with repeated Attributes. " +
                "In this case: %s.";
        sDbNeedsAtLeastOneInstance = "A %s instance needs at least one " +
                "instance.";
        sDbNeedsAtLeastOneAttribute = "A %s instance needs at least one " +
                "attribute.";
        sNoAttributesFound = "No attributes found to build the database.";
        sNoInstancesFound = "No instances found to build the database.";
        sDbReadExceptInnerMsg = "Error while reading the database. %s: %s.";
        sAttributeNameNotFound = "Attribute with name '%s' was not found.";
    }
}
