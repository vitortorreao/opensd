package io.opensd.data;

public interface Attribute {
    int getIndex();
    String getName();
    int getNumDistinctValues();
    String getStringValueAt(int valueIndex);
    int getValueIndex(String stringValue);
}
