package io.opensd.data;

public class SequentialDatabase implements Database {

    private Instance[] mInstances;
    private Attribute[] mAttributes;

    SequentialDatabase(Instance[] instances, Attribute[] attributes) {
        if (instances == null || instances.length < 1) {
            throw new IllegalArgumentException(String.format(
                    Messages.sDbNeedsAtLeastOneInstance,
                    SequentialDatabase.class.getName()));
        }
        mInstances = instances;
        if (attributes == null || attributes.length < 1) {
            throw new IllegalArgumentException(String.format(
                    Messages.sDbNeedsAtLeastOneAttribute,
                    SequentialDatabase.class.getName()));
        }
        mAttributes = attributes;
    }

    public int getNumAttributes() {
        return mAttributes.length;
    }

    public int getNumDistinctValues(int attributeIndex) {
        return mAttributes[attributeIndex].getNumDistinctValues();
    }

    public int getNumInstances() {
        return mInstances.length;
    }

    public Instance getInstanceAt(int index) {
        return mInstances[index];
    }

    public Attribute getAttributeAt(int index) {
        return mAttributes[index];
    }
}
