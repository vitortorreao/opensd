package io.opensd.data;

import java.io.Reader;

public interface DatabaseReader {
    Database read(Reader reader) throws DatabaseReadException;
}
