package io.opensd.data;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CsvReader implements DatabaseReader {

    @Override
    public Database read(Reader reader) throws DatabaseReadException {
        List<Instance> instanceList = new ArrayList<>();
        // Create the parser
        CSVParser parser;
        try {
            parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);
        } catch (IOException ex) {
            throw new DatabaseReadException(ex);
        }
        // Create map from Feature Name to Feature Index
        Map<String, Integer> name2Index = parser.getHeaderMap();
        int numFeatures = name2Index.size(); // total number of features
        if (numFeatures < 1) {
            throw new DatabaseReadException(Messages.sNoAttributesFound);
        }
        SequentialAttribute[] features = new SequentialAttribute[numFeatures];
        // Create the Features
        for (String featureName : name2Index.keySet()) {
            int idx = name2Index.get(featureName);
            features[idx] = new SequentialAttribute(idx, featureName);
        }
        // Create the Instances
        for (CSVRecord record : parser) {
            int[] featureVector = new int[numFeatures];
            for (int i = 0; i < numFeatures; i++) {
                String value = record.get(i);
                featureVector[i] = features[i].getOrAddValue(value);
            }
            instanceList.add(new SequentialInstance(features, featureVector));
        }
        try {
            parser.close();
        } catch (IOException ex) {
            throw new DatabaseReadException(ex);
        }
        int numInstances = instanceList.size();
        if (numInstances < 1) {
            throw new DatabaseReadException(Messages.sNoInstancesFound);
        }
        Instance[] instances = new Instance[numInstances];
        return new SequentialDatabase(instanceList.toArray(instances), features);
    }
}
