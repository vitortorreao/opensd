package io.opensd.data;

public interface Database {
    Instance getInstanceAt(int index);
    Attribute getAttributeAt(int index);
    int getNumAttributes();
    int getNumDistinctValues(int attributeIndex);
    int getNumInstances();
}
