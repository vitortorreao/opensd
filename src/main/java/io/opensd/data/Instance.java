package io.opensd.data;

public interface Instance {
    String getStringValue(int attributeIndex);
    String getStringValue(String attributeName);
    int getValueIndex(int attributeIndex);
    int getValueIndex(String attributeName);
    int getNumAttributes();
}
