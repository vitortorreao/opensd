package io.opensd.data;

import java.util.HashMap;
import java.util.Map;

public class SequentialInstance implements Instance {

    private int[] mValueIndexes;
    private Attribute[] mAttributes;
    private Map<String, Integer> mAttributeName2Index;

    SequentialInstance(Attribute[] attributes, int[] valueIndexes) {
        if (attributes == null || valueIndexes == null) {
            throw new IllegalArgumentException();
        }
        if (attributes.length != valueIndexes.length) {
            throw new IllegalArgumentException(String.format(
                    Messages.sInvalidInstanceArgLengths, attributes.length,
                    valueIndexes.length));
        }
        for (int i = 0; i < attributes.length; i++) {
            if (valueIndexes[i] >= attributes[i].getNumDistinctValues()) {
                throw new IndexOutOfBoundsException(String.format(
                        Messages.sInvalidInstanceValueIndex,
                        valueIndexes[i],
                        attributes[i].getName(),
                        attributes[i].getNumDistinctValues() - 1)
                );
            }
            if (valueIndexes[i] < 0) {
                throw new IndexOutOfBoundsException(String.format(
                        Messages.sNegativeInstanceValueIndex,
                        attributes[i].getName(),
                        valueIndexes[i]
                ));
            }
        }
        mAttributes = attributes;
        mValueIndexes = valueIndexes;
        mAttributeName2Index = new HashMap<>();
        for (Attribute attr : attributes) {
            if (mAttributeName2Index.put(attr.getName(), attr.getIndex())
                    != null) {
                throw new IllegalArgumentException(String.format(
                        Messages.sRepeatedAttributes,
                        SequentialInstance.class.getName(),
                        attr.getName()
                ));
            }
        }
    }

    @Override
    public String getStringValue(int attributeIndex) {
        return mAttributes[attributeIndex]
                .getStringValueAt(mValueIndexes[attributeIndex]);
    }

    @Override
    public String getStringValue(String attributeName) {
        return getStringValue(getAttributeIndexFromName(attributeName));
    }

    @Override
    public int getValueIndex(int attributeIndex) {
        return mValueIndexes[attributeIndex];
    }

    @Override
    public int getValueIndex(String attributeName) {
        return getValueIndex(getAttributeIndexFromName(attributeName));
    }

    @Override
    public int getNumAttributes() {
        return mValueIndexes.length;
    }

    private int getAttributeIndexFromName(String attributeName) {
        Integer index = mAttributeName2Index.get(attributeName);
        if (index == null) {
            throw new IllegalArgumentException(String.format(
                    Messages.sAttributeNameNotFound, attributeName));
        }
        return index;
    }
}
