package io.opensd.data;

public class DatabaseReadException extends Exception {

    private Exception mInner;

    public DatabaseReadException(String message) {
        super(message);
        mInner = null;
    }

    public DatabaseReadException(Exception innerException) {
        super(innerException.getMessage());
        mInner = innerException;
    }

    public Exception getInnerException() {
        return mInner;
    }

    @Override
    public String getMessage() {
        if (mInner == null) {
            return super.getMessage();
        } else {
            return String.format(Messages.sDbReadExceptInnerMsg,
                    mInner.getClass().getName(),
                    mInner.getMessage());
        }
    }
}
