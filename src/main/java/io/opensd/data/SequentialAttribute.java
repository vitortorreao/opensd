package io.opensd.data;

import java.util.HashMap;
import java.util.Map;

public class SequentialAttribute implements Attribute {

    private Map<String, Integer> mValue2Index;
    private Map<Integer, String> mIndex2Value;
    private String mName;
    private int mIndex;
    private int mCurrValueIndex;

    SequentialAttribute(int index, String name) {
        if (index < 0) {
            throw new IllegalArgumentException(
                    String.format(Messages.sInvalidAttributeIndex, index));
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(Messages.sInvalidAttributeName);
        }

        mValue2Index = new HashMap<>();
        mIndex2Value = new HashMap<>();
        mIndex = index;
        mName = name;
        mCurrValueIndex = 0;
    }

    int getOrAddValue(String value) {
        if (mValue2Index.containsKey(value)) {
            return mValue2Index.get(value);
        } else {
            mValue2Index.put(value, mCurrValueIndex);
            mIndex2Value.put(mCurrValueIndex, value);
            return mCurrValueIndex++;
        }
    }

    @Override
    public int getIndex() {
        return mIndex;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int getNumDistinctValues() {
        return mValue2Index.size();
    }

    @Override
    public String getStringValueAt(int valueIndex) {
        if (mIndex2Value.containsKey(valueIndex)) {
            return mIndex2Value.get(valueIndex);
        }
        return null;
    }

    @Override
    public int getValueIndex(String stringValue) {
        if (mValue2Index.containsKey(stringValue)) {
            return mValue2Index.get(stringValue);
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SequentialAttribute)) {
            return false;
        }
        SequentialAttribute other = (SequentialAttribute) obj;
        return other.getIndex() == getIndex();
    }

    @Override
    public int hashCode() {
        return getIndex();
    }
}
