package io.opensd.metrics;

final class Messages {

    static final String sInvalidBooleanOutputNodeType;
    static final String sExpectedBoolValueNode;
    static final String sBooleanNodeNotEnoughChildren;
    static final String sBooleanNodeTooManyChildren;
    static final String sInvalidNodeType;
    static final String sInvalidValueNodeType;
    static final String sInvalidComparisonOperation;
    static final String sCannotCompareCells;
    static final String sInvalidTypeForOperation;
    static final String sCannotCastCellType;
    static final String sCannotParseCellType;
    static final String sAttributeNotFound;
    static final String sNullDatabaseError;
    static final String sEmptyDatabaseError;

    static {
        sInvalidBooleanOutputNodeType = "Found a node of type %s, which " +
                "isn't acceptable as a boolean output node.";
        sExpectedBoolValueNode = "Expected a bool value node, but found a %s " +
                "value node.";
        sBooleanNodeNotEnoughChildren = "Expected at least %d child for this " +
                "boolean node, but found %d.";
        sBooleanNodeTooManyChildren = "Expected node to have one child, but " +
                "found %d children.";
        sInvalidNodeType = "Invalid note type: %s.";
        sInvalidValueNodeType = "Invalid ValueNode type: %s.";
        sInvalidComparisonOperation = "Invalid comparison operation: %s.";
        sCannotCompareCells = "Cannot compare cells of types %s and %s.";
        sInvalidTypeForOperation = "Type %s is invalid for operation %s.";
        sCannotCastCellType = "Cannot cast type %s to %s.";
        sCannotParseCellType = "Cannot parse a %s value from string: %s.";
        sAttributeNotFound = "Could not find a value for attribute '%s' in " +
                "the given instance.";
        sNullDatabaseError = "Database cannot be null.";
        sEmptyDatabaseError = "Database must have at least one instance.";
    }
}
