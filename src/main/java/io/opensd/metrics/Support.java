package io.opensd.metrics;

import io.opensd.data.Database;
import io.opensd.data.Instance;
import io.opensd.rules.Rule;

import java.util.Collection;

/**
 * This class implements the Support metric as defined by Lavrač (2005).
 * Support is defined as the relative frequency of correctly covered examples
 * of the target class in the examples set.
 *
 * In the case of io.opensd.rules implementation, this is the number of
 * instances fully covered by rule, divided by the number of instances in the
 * database.
 *
 * Reference:
 * Nada Lavrač. 2005. Subgroup discovery techniques and applications.
 * DOI=https://dx.doi.org/10.1007/11430919_2
 *
 * @author vat@cin.ufpe.br
 * @version 0.1
 * @since 0.1
 */
public final class Support implements RuleMetric, AverageMetric, OverallMetric {

    private Database mDatabase;
    private RuleVisitor mVisitor;

    public Support(Database database) {
        if (database == null) {
            throw new IllegalArgumentException(Messages.sNullDatabaseError);
        }
        mDatabase = database;
        mVisitor = new DefaultVisitor();
    }

    public Database getDatabase() {
        return mDatabase;
    }

    @Override
    public double calculate(Rule rule) {
        int totalInstances = mDatabase.getNumInstances();
        if (totalInstances < 1) return 0d;
        int tp = 0;
        for (int i = 0; i < totalInstances; i++) {
            if (mVisitor.isFullyCovered(mDatabase.getInstanceAt(i), rule)) {
                tp++;
            }
        }
        return tp / (double) totalInstances;
    }

    @Override
    public double calculateAverage(Collection<Rule> rules) {
        double sum = 0d;
        int size = 0;
        for (Rule rule : rules) {
            sum += calculate(rule);
            size++;
        }
        if (size < 1) return 0d;
        return sum / size;
    }

    @Override
    public double calculateOverall(Collection<Rule> rules) {
        int totalInstances = mDatabase.getNumInstances();
        if (totalInstances < 1) return 0d;
        int tp = 0;
        for (int i = 0; i < totalInstances; i++) {
            Instance instance = mDatabase.getInstanceAt(i);
            boolean fullyCovered = false;
            for (Rule rule : rules ) {
                if (mVisitor.isFullyCovered(instance, rule)) {
                    fullyCovered = true;
                }
            }
            if (fullyCovered) tp++;
        }
        return tp / (double) totalInstances;
    }
}
