package io.opensd.metrics;

import io.opensd.data.Database;
import io.opensd.data.Instance;
import io.opensd.rules.Rule;

import java.util.Collection;

/**
 * This class implements the Coverage metric as defined by Lavrač et al. (2004).
 * Coverage is defined as the fraction of examples covered by the antecedent
 * part of the rule.
 *
 * In terms of io.opensd.metrics implementation, this means the coverage is
 * computed as the number of instances covered by the antecedent of the rule,
 * divided by the total number of instances in the database.
 *
 * Reference:
 * Nada Lavrač, Branko Kavšek, Peter Flach, and Ljupčo Todorovski. 2004.
 * Subgroup Discovery with CN2-SD.
 * DOI=https://doi.org/10.1016/j.eswa.2007.08.083
 *
 * @author vat@cin.ufpe.br
 * @version 0.1
 * @since 0.1
 */
public final class Coverage implements RuleMetric, AverageMetric, OverallMetric {

    private Database mDatabase;
    private RuleVisitor mVisitor;

    public Coverage(Database database) {
        if (database == null) {
            throw new IllegalArgumentException(Messages.sNullDatabaseError);
        }
        mDatabase = database;
        mVisitor = new DefaultVisitor();
    }

    public Database getDatabase() {
        return mDatabase;
    }

    @Override
    public double calculate(Rule rule) {
        int cov = 0;
        int totalInstances = mDatabase.getNumInstances();
        if (totalInstances < 1) return 0d;
        for (int i = 0; i < totalInstances; i++) {
            if (mVisitor.isCoveredByAntecedent(
                    mDatabase.getInstanceAt(i), rule)) {
                cov++;
            }
        }
        return cov / (double) totalInstances;
    }

    @Override
    public double calculateAverage(Collection<Rule> rules) {
        double sum = 0d;
        int size = 0;
        for (Rule rule : rules) {
            sum += calculate(rule);
            size++;
        }
        if (size < 1) return 0d;
        return sum / size;
    }

    @Override
    public double calculateOverall(Collection<Rule> rules) {
        int cov = 0;
        int totalInstances = mDatabase.getNumInstances();
        if (totalInstances < 1) return 0d;
        for (int i = 0; i < totalInstances; i++) {
            Instance instance = mDatabase.getInstanceAt(i);
            boolean covered = false;
            for (Rule rule : rules) {
                if (mVisitor.isCoveredByAntecedent(instance, rule)) {
                    covered = true;
                }
            }
            if (covered) cov++;
        }
        return cov / (double) totalInstances;
    }
}
