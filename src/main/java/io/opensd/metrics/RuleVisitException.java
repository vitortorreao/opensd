package io.opensd.metrics;

public class RuleVisitException extends RuntimeException {

    public RuleVisitException(String message) {
        super(message);
    }
}
