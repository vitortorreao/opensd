package io.opensd.metrics;

import io.opensd.rules.Rule;

public interface RuleMetric {
    double calculate(Rule rule);
}
