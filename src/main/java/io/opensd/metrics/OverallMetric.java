package io.opensd.metrics;

import io.opensd.rules.Rule;

import java.util.Collection;

public interface OverallMetric {
    double calculateOverall(Collection<Rule> rules);
}
