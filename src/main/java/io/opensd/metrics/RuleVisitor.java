package io.opensd.metrics;

import io.opensd.data.Instance;
import io.opensd.rules.Rule;

public interface RuleVisitor {
    boolean isCoveredByAntecedent(Instance instance, Rule rule);
    boolean isCoveredByConsequent(Instance instance, Rule rule);
    boolean isFullyCovered(Instance instance, Rule rule);
}
