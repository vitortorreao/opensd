package io.opensd.metrics;

import io.opensd.data.Instance;
import io.opensd.rules.*;

public class DefaultVisitor implements RuleVisitor {

    @Override
    public boolean isCoveredByAntecedent(Instance instance, Rule rule) {
        return visitNodeWithBoolOutput(instance, rule.getAntecedent());
    }

    @Override
    public boolean isCoveredByConsequent(Instance instance, Rule rule) {
        return visitNodeWithBoolOutput(instance, rule.getConsequent());
    }

    @Override
    public boolean isFullyCovered(Instance instance, Rule rule) {
        return isCoveredByAntecedent(instance, rule)
                && isCoveredByConsequent(instance, rule);
    }

    private boolean visitNodeWithBoolOutput(Instance instance, RuleNode node) {
        switch (node.getNodeType()) {
            case COMPARISON:
                ComparisonNode compNode = (ComparisonNode) node;
                return visitComparisonNode(instance, compNode);
            case BOOLEAN:
                BooleanNode booleanNode = (BooleanNode) node;
                return visitBooleanNode(instance, booleanNode);
            case VALUE:
                ValueNode valueNode = (ValueNode) node;
                return visitBoolValueNode(valueNode);
            case ATTRIBUTE:
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidBooleanOutputNodeType,
                        node.getNodeType().name()));
        }
    }

    boolean visitBoolValueNode(ValueNode valueNode) {
        if (valueNode.getValueType() != ValueNode.Type.BOOLEAN) {
            throw new RuleVisitException(String.format(
                    Messages.sExpectedBoolValueNode,
                    valueNode.getValueType().name()));
        }
        BoolValueNode boolNode = (BoolValueNode) valueNode;
        return boolNode.getBoolValue();
    }

    boolean visitBooleanNode(Instance instance, BooleanNode node) {
        int num_operands = node.size();
        if (num_operands < 1) {
            throw new RuleVisitException(String.format(
                    Messages.sBooleanNodeNotEnoughChildren, 1,
                    num_operands));
        }
        BooleanNode.Operation op = node.getOperation();
        if (op == BooleanNode.Operation.NOT) {
            if (num_operands > 1) {
                throw new RuleVisitException(String.format(
                        Messages.sBooleanNodeTooManyChildren, num_operands));
            }
            return !visitNodeWithBoolOutput(instance, node.getChildAt(0));
        } else if (num_operands < 2) {
            throw new RuleVisitException(String.format(
                    Messages.sBooleanNodeNotEnoughChildren, 2,
                    num_operands));
        } else if (op == BooleanNode.Operation.AND) {
            boolean output = visitNodeWithBoolOutput(
                    instance, node.getChildAt(0));
            for (int i = 1; i < num_operands; i++) {
                if (!output) return false; // short circuit
                output = output &&
                        visitNodeWithBoolOutput(instance, node.getChildAt(i));
            }
            return output;
        } else {
            boolean output = visitNodeWithBoolOutput(
                    instance, node.getChildAt(0));
            for (int i = 0; i < num_operands; i++) {
                if (output) return true; // short circuit
                output = output ||
                        visitNodeWithBoolOutput(instance, node.getChildAt(i));
            }
            return output;
        }
    }

    boolean visitComparisonNode(Instance instance, ComparisonNode compNode) {
        ValueCell left = visitAnyOutputNode(instance, compNode.getLeft());
        ValueCell right = visitAnyOutputNode(instance, compNode.getRight());
        switch (compNode.getOperation()) {
            case EQUAL:
                return visitEqual(left, right);
            case NOT_EQUAL:
                return !visitEqual(left, right);
            case LESSER_THAN:
                return visitLesserThan(left, right);
            case GREATER_THAN:
                return visitGreaterThan(left, right);
            case LESSER_THAN_OR_EQUAL:
                return !visitGreaterThan(left, right);
            case GREATER_THAN_OR_EQUAL:
                return !visitLesserThan(left, right);
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidComparisonOperation,
                        compNode.getOperation().name()));
        }
    }

    private ValueCell visitAnyOutputNode(Instance instance, RuleNode node) {
        switch (node.getNodeType()) {
            case COMPARISON:
                ComparisonNode compNode = (ComparisonNode) node;
                return new ValueCell(visitComparisonNode(instance, compNode));
            case BOOLEAN:
                BooleanNode boolNode = (BooleanNode) node;
                return new ValueCell(visitBooleanNode(instance, boolNode));
            case ATTRIBUTE:
                AttributeNode attrNode = (AttributeNode) node;
                return visitAttributeNode(instance, attrNode);
            case VALUE:
                ValueNode valueNode = (ValueNode) node;
                return visitValueNode(valueNode);
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidNodeType, node.getNodeType().name()));
        }
    }

    ValueCell visitAttributeNode(Instance instance, AttributeNode attrNode) {
        String value;
        try {
            value = instance.getStringValue(attrNode.getName());
        } catch (IllegalArgumentException ex) {
            throw new RuleVisitException(String.format(
                    Messages.sAttributeNotFound, attrNode.getName()));
        }
        return new ValueCell(value);
    }

    ValueCell visitValueNode(ValueNode valueNode) {
        switch (valueNode.getValueType()) {
            case STRING:
                StringValueNode stringNode = (StringValueNode) valueNode;
                return new ValueCell(stringNode.getStringValue());
            case BOOLEAN:
                BoolValueNode boolNode = (BoolValueNode) valueNode;
                return new ValueCell(boolNode.getBoolValue());
            case FLOAT:
                FloatValueNode floatNode = (FloatValueNode) valueNode;
                return new ValueCell(floatNode.getFloatValue());
            case INTEGER:
                IntValueNode intNode = (IntValueNode) valueNode;
                return new ValueCell(intNode.getIntValue());
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidValueNodeType,
                        valueNode.getValueType().name()));
        }
    }

    boolean visitEqual(ValueCell left, ValueCell right) {
        CellType expType = calculateExpType(left.getType(), right.getType());
        switch (expType) {
            case BOOL:
                return left.getBoolValue() == right.getBoolValue();
            case FLOAT:
                return left.getFloatValue() == right.getFloatValue();
            case STRING:
                return left.getStringValue().equals(right.getStringValue());
            case INTEGER:
                return left.getIntValue() == right.getIntValue();
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidTypeForOperation,
                        expType.name(), "="));
        }
    }

    boolean visitLesserThan(ValueCell left, ValueCell right) {
        CellType expType = calculateExpType(left.getType(), right.getType());
        switch (expType) {
            case FLOAT:
                return left.getFloatValue() < right.getFloatValue();
            case INTEGER:
                return left.getIntValue() < right.getIntValue();
            case BOOL:
            case STRING:
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidTypeForOperation,
                        expType.name(), "<"));
        }
    }

    boolean visitGreaterThan(ValueCell left, ValueCell right) {
        CellType expType = calculateExpType(left.getType(), right.getType());
        switch (expType) {
            case FLOAT:
                return left.getFloatValue() > right.getFloatValue();
            case INTEGER:
                return left.getIntValue() > right.getIntValue();
            case BOOL:
            case STRING:
            default:
                throw new RuleVisitException(String.format(
                        Messages.sInvalidTypeForOperation,
                        expType.name(), ">"));
        }
    }

    CellType calculateExpType(CellType left, CellType right) {
        if (left == right)  {
            return left;
        }
        if (left == CellType.STRING) {
            return right;
        } else if (right == CellType.STRING) {
            return left;
        }
        if (left == CellType.BOOL || right == CellType.BOOL) {
            throw new RuleVisitException(String.format(
                    Messages.sCannotCompareCells, left, right));
        }
        if (left == CellType.FLOAT || right == CellType.FLOAT) {
            return CellType.FLOAT;
        }
        if (left == CellType.INTEGER || right == CellType.INTEGER) {
            return CellType.INTEGER;
        }
        throw new RuleVisitException(String.format(Messages.sCannotCompareCells,
                left, right));
    }

    enum CellType {
        BOOL, INTEGER, FLOAT, STRING
    }

    final static class ValueCell {
        private boolean mBool;
        private long mInteger;
        private double mFloat;
        private String mString;
        private CellType mType;

        ValueCell(boolean value) {
            mBool = value;
            mType = CellType.BOOL;
        }

        ValueCell(long value) {
            mInteger = value;
            mFloat = (double) value;
            mType = CellType.INTEGER;
        }

        ValueCell(double value) {
            mFloat = value;
            mType = CellType.FLOAT;
        }

        ValueCell(String value) {
            mString = value;
            mType = CellType.STRING;
        }

        CellType getType() {
            return mType;
        }

        long getIntValue() {
            if (mType == CellType.STRING) {
                return Integer.parseInt(mString);
            } else if (mType == CellType.INTEGER) {
                return mInteger;
            } else {
                throw new RuleVisitException(String.format(
                        Messages.sCannotCastCellType, mType.name(),
                        CellType.INTEGER));
            }
        }

        double getFloatValue() {
            if (mType == CellType.INTEGER) {
                return (double) mInteger;
            } else if (mType == CellType.STRING) {
                return Double.parseDouble(mString);
            } else if (mType == CellType.FLOAT) {
                return mFloat;
            } else {
                throw new RuleVisitException(String.format(
                        Messages.sCannotCastCellType, mType.name(),
                        CellType.FLOAT));
            }
        }

        String getStringValue() {
            if (mType != CellType.STRING) {
                throw new RuleVisitException(String.format(
                        Messages.sCannotCastCellType, mType.name(),
                        CellType.STRING));
            }
            return mString;
        }

        boolean getBoolValue() {
            if (mType == CellType.STRING) {
                if (mString.equalsIgnoreCase("true")) {
                    return true;
                }
                if (mString.equalsIgnoreCase("false")) {
                    return false;
                }
                else {
                    throw new RuleVisitException(String.format(
                            Messages.sCannotParseCellType, CellType.BOOL,
                            mString));
                }
            } else if (mType == CellType.BOOL) {
                return mBool;
            } else {
                throw new RuleVisitException(String.format(
                        Messages.sCannotCastCellType, mType.name(),
                        CellType.BOOL));
            }
        }
    }
}
