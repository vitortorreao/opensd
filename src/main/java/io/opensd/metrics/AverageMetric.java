package io.opensd.metrics;

import io.opensd.rules.Rule;

import java.util.Collection;

public interface AverageMetric {
    double calculateAverage(Collection<Rule> rules);
}
