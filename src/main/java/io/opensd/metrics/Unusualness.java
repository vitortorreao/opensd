package io.opensd.metrics;

import io.opensd.data.Database;
import io.opensd.data.Instance;
import io.opensd.rules.Rule;

import java.util.Collection;

/**
 * This class implements the Weighted Relative Accuracy metric as defined by
 * Lavrač, Flach and Zupan (1999). The unusualness of a subgroup, as calculated
 * by WRAcc, is a trade-off between the coverage and the accuracy gain of a
 * subgroup.
 *
 * Reference:
 * Nada Lavrač, Peter Flach, Blaz Zupan. 1999.
 * Rule Evaluation Measures: A Unifying View.
 * DOI=https://doi.org/10.1007/3-540-48751-4_17
 *
 * @author vat@cin.ufpe.br
 * @version 0.1
 * @since 0.1
 */
public final class Unusualness implements RuleMetric, AverageMetric {

    private Database mDatabase;
    private RuleVisitor mVisitor;
    private int mTotalInstances;

    public Unusualness(Database database) {
        if (database == null) {
            throw new IllegalArgumentException(Messages.sNullDatabaseError);
        }
        mDatabase = database;
        mVisitor = new DefaultVisitor();
        mTotalInstances = mDatabase.getNumInstances();
        if (mTotalInstances < 1) {
            throw new IllegalArgumentException(Messages.sEmptyDatabaseError);
        }
    }

    public Database getDatabase() {
        return mDatabase;
    }

    @Override
    public double calculateAverage(Collection<Rule> rules) {
        double sum = 0d;
        int size = 0;
        for (Rule rule : rules) {
            sum += calculate(rule);
            size++;
        }
        if (size < 1) return 0d;
        return sum / size;
    }

    @Override
    public double calculate(Rule rule) {
        int tp = 0;
        int fp = 0;
        int posInstances = 0;
        for (int i = 0; i < mTotalInstances; i++) {
            Instance instance = mDatabase.getInstanceAt(i);
            boolean antecedent = mVisitor.isCoveredByAntecedent(instance, rule);
            boolean consequent = mVisitor.isCoveredByConsequent(instance, rule);
            if (antecedent && consequent) {
                tp++;
            } else if (antecedent) {
                fp++;
            }
            if (consequent) posInstances++;
        }
        double cov = tp + fp;
        double factor1 = cov / mTotalInstances;
        double factor2 = (tp / cov) - (posInstances / (double) mTotalInstances);
        return factor1 * factor2;
    }
}
