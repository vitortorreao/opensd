package io.opensd.apps;

import io.opensd.data.CsvReader;
import io.opensd.data.Database;
import io.opensd.data.DatabaseReadException;
import io.opensd.metrics.Coverage;
import io.opensd.metrics.RuleMetric;
import io.opensd.metrics.Support;
import io.opensd.metrics.Unusualness;
import io.opensd.rules.Rule;
import io.opensd.rules.parser.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.util.List;
import java.util.Locale;

public class RuleCalculator {

    public static void main(String[] args) throws IOException,
            DatabaseReadException, LexerException, SyntaxException {
        Options options = new Options();
        options.addRequiredOption("d", "data", true,
                "The database file which will be used.");
        options.addOption("i", "input", true,
                "If present, will read from the given file path, " +
                        "instead of standard input.");
        options.addOption("o", "output", true,
                "If present, will write to the given file path, " +
                        "instead of standard output.");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
             cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar RuleCalc.jar", options);
            return;
        }

        Reader databaseReader = new FileReader(cmd.getOptionValue("data"));
        Reader ruleReader;
        Writer outWriter;
        if (cmd.hasOption("input")) {
            ruleReader = new FileReader(cmd.getOptionValue("input"));
        } else {
            ruleReader = new InputStreamReader(System.in);
        }
        if (cmd.hasOption("output")) {
            outWriter = new FileWriter(cmd.getOptionValue("output"));
        } else {
            outWriter = new OutputStreamWriter(System.out);
        }

        try {
            Database database = new CsvReader().read(databaseReader);
            Lexer lexer = new TokenReader(ruleReader);
            RuleParser ruleParser = new RuleParser(lexer);
            CSVPrinter csvPrinter = new CSVPrinter(outWriter, CSVFormat.DEFAULT
                    .withHeader("Rule #", "Support", "Coverage", "WRAcc"));
            csvPrinter.flush();
            Rule rule;
            int count = 0;
            while ((rule = ruleParser.parseNextRule()) != null) {
                calculateRuleMetrics(csvPrinter, database, rule, count);
                csvPrinter.flush();
                count++;
            }
        } finally {
            databaseReader.close();
            ruleReader.close();
            outWriter.close();
        }
    }

    private static void calculateRules(CSVPrinter csvPrinter,
                                       Database database, List<Rule> rules)
            throws IOException {
        RuleMetric[] metrics = new RuleMetric[] {
                new Support(database),
                new Coverage(database),
                new Unusualness(database)
        };
        for (int i = 0; i < rules.size(); i++) {
            String[] fields = new String[metrics.length + 1];
            fields[0] = String.format("%d", i);
            int fi = 1;
            for (RuleMetric metric : metrics) {
                fields[fi] = String.format(Locale.US, "%f",
                        metric.calculate(rules.get(i)));
                fi++;
            }
            csvPrinter.printRecord(fields);
        }
    }

    private static void calculateRuleMetrics(CSVPrinter csvPrinter,
                                             Database database, Rule rule,
                                             int count)
            throws IOException {
        RuleMetric[] metrics = new RuleMetric[] {
                new Support(database),
                new Coverage(database),
                new Unusualness(database)
        };
        String[] fields = new String[metrics.length + 1];
        fields[0] = String.format("%d", count);
        for (int fi = 1; fi <= metrics.length; fi++) {
            fields[fi] = String.format(Locale.US, "%f",
                    metrics[fi - 1].calculate(rule));
        }
        csvPrinter.printRecord(fields);
    }
}
