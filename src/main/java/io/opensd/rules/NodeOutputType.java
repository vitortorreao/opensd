package io.opensd.rules;

public enum NodeOutputType {
    UNKNOWN,
    BOOLEAN,
    STRING,
    NUMBER
}
