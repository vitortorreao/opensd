package io.opensd.rules;

public interface RuleNode {
    NodeType getNodeType();
    NodeOutputType getOutputType();
}
