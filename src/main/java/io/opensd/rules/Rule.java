package io.opensd.rules;

public class Rule {

    private RuleNode mAntecedent, mConsequent;

    public Rule(RuleNode antecedent, RuleNode consequent) {
        if (antecedent == null) {
            throw new IllegalArgumentException(Messages.sNullAntecedent);
        }
        if (antecedent.getOutputType() != NodeOutputType.BOOLEAN) {
            String message = String.format(Messages.sInvalidChildNodeOutputType,
                    getClass().getName(), NodeOutputType.BOOLEAN);
            throw new IllegalArgumentException(message);
        }
        mAntecedent = antecedent;
        if (consequent == null) {
            throw new IllegalArgumentException(Messages.sNullConsequent);
        }
        if (consequent.getOutputType() != NodeOutputType.BOOLEAN) {
            String message = String.format(Messages.sInvalidChildNodeOutputType,
                    getClass().getName(), NodeOutputType.BOOLEAN);
            throw new IllegalArgumentException(message);
        }
        mConsequent = consequent;
    }

    public RuleNode getAntecedent() {
        return mAntecedent;
    }

    public RuleNode getConsequent() {
        return mConsequent;
    }
}
