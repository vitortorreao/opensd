package io.opensd.rules;

public class ComparisonNode implements RuleNode {

    public enum Operation {
        EQUAL,
        NOT_EQUAL,
        LESSER_THAN,
        LESSER_THAN_OR_EQUAL,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL
    }

    private Operation mOperation;
    private RuleNode mLeft, mRight;

    public ComparisonNode(Operation op, RuleNode left, RuleNode right) {
        mOperation = op;
        if (left == null || right == null) {
            throw new IllegalArgumentException(Messages.sNullChild);
        }
        mLeft = left;
        mRight = right;
    }

    public NodeType getNodeType() {
        return NodeType.COMPARISON;
    }

    public Operation getOperation() {
        return mOperation;
    }

    public RuleNode getLeft() {
        return mLeft;
    }

    public RuleNode getRight() {
        return mRight;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.BOOLEAN;
    }
}
