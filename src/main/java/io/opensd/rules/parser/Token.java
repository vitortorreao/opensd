package io.opensd.rules.parser;

public class Token {

    private TokenType mType;
    private String mLexeme;
    private int mLineNumber, mStartCol, mEndCol;

    public Token(TokenType type, String lexeme, int lineNumber, int startCol,
                 int endCol) {
        mType = type;
        if (lexeme == null || (lexeme.isEmpty() && type != TokenType.STRING)) {
            throw new IllegalArgumentException(Messages.sTokenInvalidArg);
        }
        mLexeme = lexeme;
        mLineNumber = lineNumber;
        mStartCol = startCol;
        mEndCol = endCol;
    }

    public TokenType getType() {
        return mType;
    }

    public String getLexeme() {
        return mLexeme;
    }

    public int getLineNumber() {
        return mLineNumber;
    }

    public int getStartCol() {
        return mStartCol;
    }

    public int getEndCol() {
        return mEndCol;
    }
}
