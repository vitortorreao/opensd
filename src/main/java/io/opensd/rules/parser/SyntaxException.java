package io.opensd.rules.parser;

public class SyntaxException extends Exception {

    private int mLineNumber, mColNumber;

    public SyntaxException(String message, int lineNumber, int colNumber) {
        super(message);
        mLineNumber = lineNumber;
        mColNumber = colNumber;
    }

    @Override
    public String getMessage() {
        return String.format(Messages.sSyntaxErrorWithLineBase, mLineNumber,
                    mColNumber, super.getMessage());
    }
}
