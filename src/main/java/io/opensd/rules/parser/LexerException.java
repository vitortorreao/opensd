package io.opensd.rules.parser;

public class LexerException extends Exception {

    private static final String sMessage = "%s %s: %s";

    private int mLineNumber;
    private int mColNumber;

    public LexerException(String message) {
        this(message, -1, -1);
    }

    public LexerException(String message, int lineNumber, int colNumber) {
        super(message);
        mLineNumber = lineNumber;
        mColNumber = colNumber;
    }

    public LexerException(String message, Exception innerException) {
        this(message, innerException, -1, -1);
    }

    public LexerException(String message, Exception innerException,
                          int lineNumber, int colNumber) {
        super(message, innerException);
        mLineNumber = lineNumber;
        mColNumber = colNumber;
    }

    public Exception getInnerException() {
        Throwable inner = getCause();
        return inner != null ? (Exception) inner : null;
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        Exception inner = getInnerException();
        return inner != null ? inner.getStackTrace() : super.getStackTrace();
    }

    @Override
    public String getMessage() {
        String message;
        if (mLineNumber < 0 || mColNumber < 0) {
            message = super.getMessage();
        } else {
            message = String.format("%s (at %d:%d).", super.getMessage(),
                    mLineNumber, mColNumber);
        }
        Exception inner = getInnerException();
        if (inner != null) {
            return String.format(sMessage, message, inner.getClass().getName(),
                    inner.getMessage());
        } else {
            return message;
        }
    }
}
