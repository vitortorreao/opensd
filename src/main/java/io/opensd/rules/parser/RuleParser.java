package io.opensd.rules.parser;

import io.opensd.rules.*;

import java.util.*;

public class RuleParser {

    public static final Set<TokenType> sCompTokens =
            new HashSet<>(Arrays.asList(TokenType.EQUAL,
                    TokenType.NOT_EQUAL,
                    TokenType.LESSER_THAN,
                    TokenType.LESSER_THAN_OR_EQUAL,
                    TokenType.GREATER_THAN,
                    TokenType.GREATER_THAN_OR_EQUAL));
    public static final Set<TokenType> sElementTokens =
            new HashSet<>(Arrays.asList(TokenType.OPEN_PARENTHESIS,
                    TokenType.NOT,
                    TokenType.TRUE,
                    TokenType.FALSE,
                    TokenType.IDENTIFIER,
                    TokenType.STRING,
                    TokenType.INTEGER,
                    TokenType.FLOAT,
                    TokenType.DASH));

    private Lexer mLexer;
    private Token mCurrentToken;
    private Token mPreviousToken;

    public RuleParser(Lexer lexer) {
        if (lexer == null) {
            throw new IllegalArgumentException(Messages.sNullLexer);
        }
        mLexer = lexer;
        mCurrentToken = null;
        mPreviousToken = null;
    }

    public Rule parseNextRule() throws LexerException, SyntaxException {
        if (mPreviousToken != null && mCurrentToken == null) return null;
        return parseRule();
    }

    public List<Rule> parseRules() throws LexerException, SyntaxException {
        List<Rule> rules = new ArrayList<>();
        Rule rule;
        while ((rule = parseRule()) != null) rules.add(rule);
        return rules;
    }

    private Rule parseRule() throws LexerException, SyntaxException {
        getNextToken();
        if (mCurrentToken == null) return null;
        RuleNode antecedent = parseBoolExp();
        validateCurrentToken(TokenType.ARROW);
        getNextToken();
        RuleNode consequent = parseBoolExp();
        validateCurrentToken(TokenType.SEMICOLON);
        return new Rule(antecedent, consequent);
    }

    private RuleNode parseBoolExp() throws LexerException, SyntaxException {
        List<RuleNode> nodes = new ArrayList<>();
        RuleNode node = parseTerm();
        if (node != null) nodes.add(node);
        while (mCurrentToken != null
                && mCurrentToken.getType() == TokenType.OR) {
            getNextToken();
            node = parseTerm();
            if (node != null) nodes.add(node);
        }
        if (nodes.size() > 1) {
            return new BooleanNode(BooleanNode.Operation.OR,
                    nodes.toArray(new RuleNode[0]));
        }
        return nodes.get(0);
    }

    private RuleNode parseTerm() throws LexerException, SyntaxException {
        List<RuleNode> nodes = new ArrayList<>();
        RuleNode node = parseFactor();
        if (node != null) nodes.add(node);
        while (mCurrentToken != null
                && mCurrentToken.getType() == TokenType.AND) {
            getNextToken();
            node = parseFactor();
            if (node != null) nodes.add(node);
        }
        if (nodes.size() > 1) {
            return new BooleanNode(BooleanNode.Operation.AND,
                    nodes.toArray(new RuleNode[0]));
        }
        return nodes.get(0);
    }

    private RuleNode parseFactor() throws LexerException, SyntaxException {
        if (mCurrentToken == null) throwUnpexpectedEndOfFeed();
        RuleNode node;
        switch (mCurrentToken.getType()) {
            case OPEN_PARENTHESIS:
            case NOT:
            case FALSE:
            case TRUE:
                node = parseBoolVal();
                if (isCurrentTokenComp()) {
                    return parseComparison(node);
                } else {
                    return node;
                }
            default:
                node = parseElement();
                validateCurrentToken(sCompTokens);
                return parseComparison(node);
        }
    }

    private RuleNode parseBoolVal() throws LexerException, SyntaxException {
        switch (mCurrentToken.getType()) {
            case NOT:
                getNextToken();
                return new BooleanNode(BooleanNode.Operation.NOT,
                        new RuleNode[] { parseBoolVal() });
            case OPEN_PARENTHESIS:
                getNextToken();
                RuleNode boolExp = parseBoolExp();
                validateCurrentToken(TokenType.CLOSE_PARENTHESIS);
                getNextToken();
                return boolExp;
            case TRUE:
            case FALSE:
                RuleNode node = new BoolValueNode(mCurrentToken.getLexeme());
                getNextToken();
                return node;
            default:
                String message = String.format(
                        Messages.sExpectedTokensButFoundNeither,
                        String.join(", ",
                                TokenType.NOT.name(),
                                TokenType.OPEN_PARENTHESIS.name(),
                                TokenType.TRUE.name(),
                                TokenType.FALSE.name()),
                        mCurrentToken.getLexeme());
                throw new SyntaxException(message,
                        mCurrentToken.getLineNumber(),
                        mCurrentToken.getStartCol());
        }
    }

    private ComparisonNode parseComparison(RuleNode leftSide)
            throws LexerException, SyntaxException {
        ComparisonNode.Operation operation;
        switch (mCurrentToken.getType()) {
            case EQUAL:
                operation = ComparisonNode.Operation.EQUAL;
                break;
            case NOT_EQUAL:
                operation = ComparisonNode.Operation.NOT_EQUAL;
                break;
            case LESSER_THAN:
                operation = ComparisonNode.Operation.LESSER_THAN;
                break;
            case LESSER_THAN_OR_EQUAL:
                operation = ComparisonNode.Operation.LESSER_THAN_OR_EQUAL;
                break;
            case GREATER_THAN:
                operation = ComparisonNode.Operation.GREATER_THAN;
                break;
            case GREATER_THAN_OR_EQUAL:
                operation = ComparisonNode.Operation.GREATER_THAN_OR_EQUAL;
                break;
            default:
                List<String> tokenNames = toTokenNames(sCompTokens);
                String message = String.format(
                        Messages.sExpectedTokensButFoundNeither,
                        String.join(", ", tokenNames),
                        mCurrentToken.getLexeme());
                throw new SyntaxException(message,
                        mCurrentToken.getLineNumber(),
                        mCurrentToken.getStartCol());
        }
        getNextToken();
        RuleNode rightSide = parseElement();
        return new ComparisonNode(operation, leftSide, rightSide);
    }

    private RuleNode parseElement() throws LexerException, SyntaxException {
        RuleNode node;
        validateCurrentTokenIsNotNull(sElementTokens);
        switch (mCurrentToken.getType()) {
            case OPEN_PARENTHESIS:
            case NOT:
            case TRUE:
            case FALSE:
                return parseBoolVal();
            case IDENTIFIER:
                node = new AttributeNode(mCurrentToken.getLexeme());
                break;
            case STRING:
                node = new StringValueNode(mCurrentToken.getLexeme());
                break;
            case INTEGER:
                node = new IntValueNode(mCurrentToken.getLexeme());
                break;
            case FLOAT:
                node = new FloatValueNode(mCurrentToken.getLexeme());
                break;
            case DASH:
                getNextToken();
                if (mCurrentToken.getType() == TokenType.FLOAT) {
                    node = new FloatValueNode("-" + mCurrentToken.getLexeme());
                } else if (mCurrentToken.getType() == TokenType.INTEGER) {
                    node = new IntValueNode("-" + mCurrentToken.getLexeme());
                } else {
                    String message = String.format(
                            Messages.sExpectedTokensButFoundNeither,
                            String.join(", ", TokenType.FLOAT.name(),
                                    TokenType.INTEGER.name()),
                            mCurrentToken.getLexeme());
                    throw new SyntaxException(message,
                            mCurrentToken.getLineNumber(),
                            mCurrentToken.getStartCol());
                }
                break;
            default:
                List<String> tokenNames = toTokenNames(sElementTokens);
                String names = String.join(", ", tokenNames);
                String message = String.format(
                        Messages.sExpectedTokensButFoundNeither,
                        names, mCurrentToken.getLexeme());
                throw new SyntaxException(message, mCurrentToken
                        .getLineNumber(),
                        mCurrentToken.getStartCol());
        }
        getNextToken();
        return node;
    }

    private boolean isCurrentTokenComp() {
        if (mCurrentToken == null) {
            return false;
        }
        switch (mCurrentToken.getType()) {
            case EQUAL:
            case NOT_EQUAL:
            case LESSER_THAN:
            case LESSER_THAN_OR_EQUAL:
            case GREATER_THAN:
            case GREATER_THAN_OR_EQUAL:
                return true;
            default:
                return false;
        }
    }

    private void validateCurrentToken(TokenType type) throws SyntaxException {
        validateCurrentTokenIsNotNull(type);
        if (mCurrentToken.getType() != type) {
            String message = String.format(Messages.sExpectedTokenButFoundOther,
                    type.name(), mCurrentToken.getLexeme());
            throw new SyntaxException(message, mCurrentToken.getLineNumber(),
                    mCurrentToken.getStartCol());
        }
    }

    private void validateCurrentTokenIsNotNull(TokenType type)
            throws SyntaxException {
        if (mCurrentToken == null) {
            String message = String.format(
                    Messages.sExpectedTokenButFoundNothing, type.name());
            if (mPreviousToken == null) {
                throw new SyntaxException(message, 1, 1);
            } else {
                throw new SyntaxException(message,
                        mPreviousToken.getLineNumber(),
                        mPreviousToken.getEndCol());
            }
        }
    }

    private void validateCurrentToken(Set<TokenType> expectedTokens)
            throws SyntaxException {
        List<String> tokenNames = toTokenNames(expectedTokens);
        String names = String.join(", ", tokenNames);
        validateCurrentTokenIsNotNull(expectedTokens);
        if (!expectedTokens.contains(mCurrentToken.getType())) {
            String message = String.format(
                    Messages.sExpectedTokensButFoundNeither,
                    names,
                    mCurrentToken.getLexeme());
            throw new SyntaxException(message,
                    mCurrentToken.getLineNumber(),
                    mCurrentToken.getStartCol());
        }
    }

    private void validateCurrentTokenIsNotNull(Set<TokenType> expectedTokens)
            throws SyntaxException {
        List<String> tokenNames = toTokenNames(expectedTokens);
        String names = String.join(", ", tokenNames);
        if (mCurrentToken == null) {
            String message = String.format(
                    Messages.sExpectedTokensButFoundNothing,
                    names);
            if (mPreviousToken == null) {
                throw new SyntaxException(message, 1, 1);
            } else {
                throw new SyntaxException(message,
                        mPreviousToken.getLineNumber(),
                        mPreviousToken.getEndCol());
            }
        }
    }

    private void throwUnpexpectedEndOfFeed() throws SyntaxException {
        if (mPreviousToken == null) {
            throw new SyntaxException(Messages.sUnexpectedFeedEnd + ".", 1, 1);
        } else {
            throw new SyntaxException(Messages.sUnexpectedFeedEnd + ".",
                    mPreviousToken.getLineNumber(),
                    mPreviousToken.getEndCol());
        }
    }

    private void getNextToken() throws LexerException {
        if (mCurrentToken != null) mPreviousToken = mCurrentToken;
        mCurrentToken = mLexer.nextToken();
    }

    private List<String> toTokenNames(Iterable<TokenType> tokens) {
        List<String> names = new ArrayList<>();
        for (TokenType type : tokens) {
            names.add(type.name());
        }
        return names;
    }
}
