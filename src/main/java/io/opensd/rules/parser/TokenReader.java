package io.opensd.rules.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class TokenReader implements Lexer {

    private static final int sDefaultBuffSize = 4096;
    private static final char[] sLowerTrueLiteral = new char[] { 't', 'r', 'u',
            'e' };
    private static final char[] sUpperTrueLiteral = new char[] { 'T', 'R', 'U',
            'E' };
    private static final char[] sLowerFalseLiteral = new char[] { 'f', 'a', 'l',
            's', 'e' };
    private static final char[] sUpperFalseLiteral = new char[] { 'F', 'A', 'L',
            'S', 'E' };

    private BufferedReader mReader;
    private int mLookAhead;
    private int mLineNumber;
    private int mColNumber;
    private int mTokenStartCol;

    public TokenReader(Reader reader) {
        this(reader, sDefaultBuffSize);
    }

    public TokenReader(Reader reader, int buffSize) {
        mReader = new BufferedReader(reader, buffSize);
        mLookAhead = -1;
        mLineNumber = 1;
        mColNumber = 0;
        mTokenStartCol = 0;
    }

    public Token nextToken() throws LexerException {
        int readResult = readNextChar();
        mTokenStartCol = mColNumber;
        if (readResult == -1) {
            return null;
        }
        char character = (char) readResult;
        StringBuilder lexeme = new StringBuilder();
        lexeme.append(character);
        switch (character) {
            case '\0':
            case ' ':
            case '\n':
            case '\t':
            case '\r':
                return nextToken();
            case '!':
                return new Token(TokenType.NOT, "!", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case '&':
                return new Token(TokenType.AND, "&", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case '|':
                return new Token(TokenType.OR, "|", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case '=':
                return new Token(TokenType.EQUAL, "=", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case ';':
                return new Token(TokenType.SEMICOLON, ";", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case '(':
                return new Token(TokenType.OPEN_PARENTHESIS, "(",
                        mLineNumber, mTokenStartCol, mTokenStartCol + 1);
            case ')':
                return new Token(TokenType.CLOSE_PARENTHESIS, ")",
                        mLineNumber, mTokenStartCol, mTokenStartCol + 1);
            case '+':
                return new Token(TokenType.PLUS, "+", mLineNumber,
                        mTokenStartCol, mTokenStartCol + 1);
            case '-':
                return buildDash();
            case '$':
                return buildDollarSign();
            case '\"':
                return buildStringLiteral();
            case '<':
                return buildLesserThan();
            case '>':
                return buildGreaterThan();
            case 't':
                return buildLiteral(TokenType.TRUE, lexeme, sLowerTrueLiteral);
            case 'T':
                return buildLiteral(TokenType.TRUE, lexeme, sUpperTrueLiteral);
            case 'f':
                return buildLiteral(TokenType.FALSE, lexeme,
                        sLowerFalseLiteral);
            case 'F':
                return buildLiteral(TokenType.FALSE, lexeme,
                        sUpperFalseLiteral);
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return buildNumber(lexeme);
            default:
                throw new LexerException(
                        String.format(Messages.sUnexpectedChar, character),
                        mLineNumber, mColNumber);
        }
    }

    private Token buildDash() throws LexerException {
        if (mLookAhead < 0) {
            return new Token(TokenType.DASH, "-", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
        char character = (char) mLookAhead;
        if (character == '>') {
            readNextChar(); // consume the look ahead
            return new Token(TokenType.ARROW, "->", mLineNumber,
                    mTokenStartCol, mColNumber + 1);
        } else {
            return new Token(TokenType.DASH, "-", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
    }

    private Token buildDollarSign() throws LexerException {
        if (mLookAhead < 0) {
            return new Token(TokenType.DOLLAR_SIGN, "$", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
        char character = (char) mLookAhead;
        if (character == '{') {
            readNextChar(); // consume the look ahead
            return buildIdentifier();
        } else {
            return new Token(TokenType.DOLLAR_SIGN, "$", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
    }

    private Token buildLesserThan() throws LexerException {
        if (mLookAhead < 0) {
            return new Token(TokenType.LESSER_THAN, "<", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
        char character = (char) mLookAhead;
        if (character == '=') {
            readNextChar(); // consume the look ahead
            return new Token(TokenType.LESSER_THAN_OR_EQUAL, "<=", mLineNumber,
                    mTokenStartCol, mColNumber + 1);
        } else if (character == '>') {
            readNextChar(); // consume the look ahead
            return new Token(TokenType.NOT_EQUAL, "<>", mLineNumber,
                    mTokenStartCol, mColNumber + 1);
        } else {
            return new Token(TokenType.LESSER_THAN, "<", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
    }

    private Token buildGreaterThan() throws LexerException {
        if (mLookAhead < 0) {
            return new Token(TokenType.GREATER_THAN, ">", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
        char character = (char) mLookAhead;
        if (character == '=') {
            readNextChar(); // consume the look ahead
            return new Token(TokenType.GREATER_THAN_OR_EQUAL, ">=",
                    mLineNumber, mTokenStartCol, mColNumber + 1);
        } else {
            return new Token(TokenType.GREATER_THAN, ">", mLineNumber,
                    mTokenStartCol, mTokenStartCol + 1);
        }
    }

    private Token buildLiteral(TokenType type, StringBuilder lexeme,
                               char[] expected) throws LexerException {
        for (int i = 1; i < expected.length; i++) {
            if (mLookAhead < 0) {
                throw new LexerException(Messages.sUnexpectedFeedEnd,
                        mLineNumber, mColNumber + 1);
            }
            char character = (char) mLookAhead;
            if (character != expected[i]) {
                throw new LexerException(
                        String.format(Messages.sNonExpectedChar, expected[i],
                                new String(expected), character),
                        mLineNumber, mColNumber + 1);
            } else {
                lexeme.append((char) readNextChar());
            }
        }
        return new Token(type, lexeme.toString(), mLineNumber,
                mTokenStartCol, mColNumber + 1);
    }

    private Token buildNumber(StringBuilder lexeme) throws LexerException {
        while (true) {
            if (mLookAhead < 0) {
                return new Token(TokenType.INTEGER, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            }
            char character = (char) mLookAhead;
            if (character >= '0' && character <= '9') {
                lexeme.append((char) readNextChar());
            } else if (character == '.') {
                lexeme.append((char) readNextChar());
                return buildFloat(lexeme);
            } else {
                return new Token(TokenType.INTEGER, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            }
        }
    }

    private Token buildFloat(StringBuilder lexeme) throws LexerException {
        boolean firstNewDigit = false;
        while (true) {
            if (mLookAhead < 0 && firstNewDigit) {
                return new Token(TokenType.FLOAT, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            } else if (mLookAhead < 0 && !firstNewDigit) {
                throw new LexerException(Messages.sUnexpectedFeedEnd,
                        mLineNumber, mColNumber + 1);
            }
            char character = (char) mLookAhead;
            if (character >= '0' && character <= '9') {
                lexeme.append((char) readNextChar());
                firstNewDigit = true;
            } else if (character == 'e' || character == 'E') {
                lexeme.append((char) readNextChar());
                return buildExponent(lexeme);
            } else if (firstNewDigit) {
                return new Token(TokenType.FLOAT, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            } else {
                throw new LexerException(String.format(Messages.sUnexpectedChar,
                        character), mLineNumber, mColNumber + 1);
            }
        }
    }

    private Token buildExponent(StringBuilder lexeme) throws LexerException {
        boolean firstNewDigit = false;
        while (true) {
            if (mLookAhead < 0 && firstNewDigit) {
                return new Token(TokenType.FLOAT, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            } else if (mLookAhead < 0 && !firstNewDigit) {
                throw new LexerException(Messages.sUnexpectedFeedEnd,
                        mLineNumber, mColNumber + 1);
            }
            char character = (char) mLookAhead;
            if (!firstNewDigit && (character == '-' || character == '+')) {
                lexeme.append((char) readNextChar());
            } else if (character >= '0' && character <= '9') {
                lexeme.append((char) readNextChar());
                firstNewDigit = true;
            } else if (firstNewDigit) {
                return new Token(TokenType.FLOAT, lexeme.toString(),
                        mLineNumber, mTokenStartCol, mColNumber + 1);
            } else {
                throw new LexerException(String.format(Messages.sUnexpectedChar,
                        character), mLineNumber, mColNumber + 1);
            }
        }
    }

    private Token buildStringLiteral() throws LexerException {
        StringBuilder lexeme = new StringBuilder();
        String interruptMessage = Messages.sUnexpectedFeedEnd;
        char character;
        while ((character = readNextChar(interruptMessage)) != '\"') {
            if (character == '\\') {
                character = readNextChar(interruptMessage);
                switch (character) {
                    case 't':
                        lexeme.append('\t');
                        continue;
                    case 'b':
                        lexeme.append('\b');
                        continue;
                    case 'n':
                        lexeme.append('\n');
                        continue;
                    case 'r':
                        lexeme.append('\r');
                        continue;
                    case 'f':
                        lexeme.append('\f');
                        continue;
                    case '\'':
                        lexeme.append('\'');
                        continue;
                    case '\"':
                        lexeme.append('\"');
                        continue;
                    case '\\':
                        lexeme.append('\\');
                        continue;
                    default:
                        throw new LexerException(String.format(
                                Messages.sInvalidCtrlSeq,
                                character), mLineNumber, mColNumber);
                }
            }
            lexeme.append(character);
        }
        return new Token(TokenType.STRING, lexeme.toString(), mLineNumber,
                mTokenStartCol, mColNumber + 1);
    }

    private Token buildIdentifier() throws LexerException {
        StringBuilder lexeme = new StringBuilder();
        String interruptMessage = Messages.sUnexpectedFeedEnd;
        char character;
        while ((character = readNextChar(interruptMessage)) != '}') {
            lexeme.append(character);
        }
        if (lexeme.length() < 1) {
            throw new LexerException(Messages.sEmptyIdentifiers, mLineNumber,
                    mColNumber);
        }
        return new Token(TokenType.IDENTIFIER, lexeme.toString(), mLineNumber,
                mTokenStartCol, mColNumber + 1);
    }

    private char readNextChar(String interruptMessage) throws LexerException {
        int next = readNextChar();
        if (next < 0) {
            throw new LexerException(interruptMessage, mLineNumber, mColNumber);
        }
        return (char) next;
    }

    private int readNextChar() throws LexerException {
        int newResult = readNextByte();
        if (newResult >= 0 && mLookAhead < 0) {
            mLookAhead = readNextByte();
        } else {
            int aux = mLookAhead;
            mLookAhead = newResult;
            newResult = aux;
        }
        if (newResult > 0 && (char) newResult == '\n') {
            mLineNumber++;
            mColNumber = 0;
        } else {
            mColNumber++;
        }
        return newResult;
    }

    private int readNextByte() throws LexerException {
        try {
            return mReader.read();
        } catch (IOException ex) {
            throw new LexerException(Messages.sFileReadError, ex,
                    mLineNumber, mColNumber + 1);
        }
    }
}
