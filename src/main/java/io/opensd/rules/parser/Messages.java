package io.opensd.rules.parser;

final class Messages {

    private Messages() {}

    static final String sTokenInvalidArg;
    static final String sUnexpectedChar;
    static final String sUnexpectedFeedEnd;
    static final String sNonExpectedChar;
    static final String sInvalidCtrlSeq;
    static final String sEmptyIdentifiers;
    static final String sFileReadError;
    static final String sAttrIsNullOrEmpty;
    static final String sSyntaxErrorWithLineBase;
    static final String sExpectedTokenButFoundNothing;
    static final String sExpectedTokenButFoundOther;
    static final String sExpectedTokensButFoundNothing;
    static final String sExpectedTokensButFoundNeither;
    static final String sUnexpectedToken;
    static final String sNullLexer;

    static {
        sTokenInvalidArg = "A null or empty lexeme is invalid.";
        sUnexpectedChar = "Unexpected character '%c'";
        sUnexpectedFeedEnd = "Unexpected end of feed";
        sNonExpectedChar = "Expected character '%c' to form token '%s', but " +
                "found '%c'";
        sInvalidCtrlSeq = "Undefined escape sequence: '\\%c'";
        sEmptyIdentifiers = "Empty identifiers are invalid";
        sFileReadError = "Cannot read next character";
        sAttrIsNullOrEmpty = "Attribute name cannot be null or empty";
        sSyntaxErrorWithLineBase = "Syntax error at line %d:%d: %s";
        sExpectedTokenButFoundNothing = "Expected a Token of type '%s', but " +
                "found none.";
        sExpectedTokenButFoundOther = "Expected token of type '%s', but found" +
                " '%s'.";
        sUnexpectedToken = "Unexpected token '%s'.";
        sExpectedTokensButFoundNothing = "Expected one of these tokens: %s. " +
                "But found none.";
        sExpectedTokensButFoundNeither = "Expected one of these tokens: %s. " +
                "But found '%s'.";
        sNullLexer = "Cannot create a parser with a null lexer.";
    }
}
