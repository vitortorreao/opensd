package io.opensd.rules.parser;

public interface Lexer {
    Token nextToken() throws LexerException;
}
