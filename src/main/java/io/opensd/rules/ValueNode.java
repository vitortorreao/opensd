package io.opensd.rules;

public abstract class ValueNode implements RuleNode {

    public enum Type {
        INTEGER,
        FLOAT,
        BOOLEAN,
        STRING
    }

    private Type mType;
    private String mValue;

    public ValueNode(Type type, String value) {
        mType = type;
        if (value == null) {
            throw new IllegalArgumentException(Messages.sNullValue);
        }
        if (type != Type.STRING && value.isEmpty()) {
            String message = String.format(Messages.sEmptyValue, type.name());
            throw new IllegalArgumentException(message);
        }
        mValue = value;
    }

    public Type getValueType() {
        return mType;
    }

    public String getStringValue() {
        return mValue;
    }

    public NodeType getNodeType() {
        return NodeType.VALUE;
    }
}
