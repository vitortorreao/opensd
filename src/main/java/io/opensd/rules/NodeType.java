package io.opensd.rules;

public enum NodeType {
    BOOLEAN,
    COMPARISON,
    VALUE,
    ATTRIBUTE
}
