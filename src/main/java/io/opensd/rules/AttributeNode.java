package io.opensd.rules;

public class AttributeNode implements RuleNode {

    private String mName;

    public AttributeNode(String attrName) {
        if (attrName == null || attrName.isEmpty()) {
            throw new IllegalArgumentException(Messages.sNullAttribute);
        }
        mName = attrName;
    }

    public String getName() {
        return mName;
    }

    public NodeType getNodeType() {
        return NodeType.ATTRIBUTE;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.UNKNOWN;
    }
}

