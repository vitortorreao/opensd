package io.opensd.rules;

public class BoolValueNode extends ValueNode {

    private boolean mValue;

    public BoolValueNode(String value) {
        super(Type.BOOLEAN, value);
        if (value.equalsIgnoreCase("true")) {
            mValue = true;
        } else if (value.equalsIgnoreCase("false")) {
            mValue = false;
        } else {
            String message = String.format(Messages.sBoolParseError, value);
            throw new IllegalArgumentException(message);
        }
    }

    public boolean getBoolValue() {
        return mValue;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.BOOLEAN;
    }
}
