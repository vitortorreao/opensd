package io.opensd.rules;

final class Messages {

    private Messages() {}

    public static final String sNullValue;
    public static final String sEmptyValue;
    public static final String sIntParseError;
    public static final String sFloatParseError;
    public static final String sBoolParseError;
    public static final String sNullAttribute;
    public static final String sNoChildrenError;
    public static final String sLessThanTwoChildrenError;
    public static final String sTooManyChildrenError;
    public static final String sNullChild;
    public static final String sInvalidNodeTypeChild;
    public static final String sInvalidValueTypeChild;
    public static final String sInvalidChildNodeOutputType;
    public static final String sNullAntecedent;
    public static final String sNullConsequent;

    static {
        sNullValue = "Cannot create a ValueNode with a null value.";
        sEmptyValue = "Cannot create a %s type ValueNode with an empty value.";
        sIntParseError = "Cannot convert value '%s' to an integer.";
        sFloatParseError = "Cannot convert value '%s' to a float.";
        sBoolParseError = "Cannot convert value '%s' to a boolean.";
        sNullAttribute = "Cannot create an AttributeNode with a null " +
                "or empty value";
        sNoChildrenError = "Cannot create a boolean node without " +
                "children nodes";
        sLessThanTwoChildrenError = "A BooleanNode with operation '%s' " +
                "cannot have less than two children.";
        sTooManyChildrenError = "A BooleanNode with operation '%s' cannot " +
                "have more than %d children.";
        sNullChild = "Children nodes cannot be null.";
        sInvalidNodeTypeChild = "A node of type '%s' cannot have a child of type '%s'.";
        sInvalidValueTypeChild = "A note of type '%s' cannot have a " +
                "ValueNode child of type '%s'.";
        sInvalidChildNodeOutputType = "A child node of '%s' must have a '%s' " +
                "output type.";
        sNullAntecedent = "A Rule cannot have a null antecedent.";
        sNullConsequent = "A Rule cannot have a null consequent.";
    }
}
