package io.opensd.rules;

public class FloatValueNode extends ValueNode {

    private double mValue;

    public FloatValueNode(String value) {
        super(Type.FLOAT, value);
        try {
            mValue = Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            String message = String.format(Messages.sFloatParseError, value);
            throw new IllegalArgumentException(message);
        }
    }

    public double getFloatValue() {
        return mValue;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.NUMBER;
    }
}
