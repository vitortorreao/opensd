package io.opensd.rules;

public class BooleanNode implements RuleNode {

    public enum Operation {
        AND,
        OR,
        NOT
    }

    private Operation mOperation;
    private RuleNode[] mChildren;

    public BooleanNode(Operation operation, RuleNode[] children) {
        mOperation = operation;
        if (children == null || children.length < 1) {
            throw new IllegalArgumentException(Messages.sNoChildrenError);
        }
        if (operation != Operation.NOT && children.length < 2) {
            String message = String.format(
                    Messages.sLessThanTwoChildrenError, operation.name());
            throw new IllegalArgumentException(message);
        }
        if (operation == Operation.NOT && children.length > 1) {
            String message = String.format(
                    Messages.sTooManyChildrenError, operation.name(), 1);
            throw new IllegalArgumentException(message);
        }
        for (int i = 0; i < children.length; i++) {
            validateChild(children[i]);
        }
        mChildren = children;
    }

    public RuleNode getChildAt(int i) {
        return mChildren[i];
    }

    public int size() {
        return mChildren.length;
    }

    public Operation getOperation() {
        return mOperation;
    }

    public NodeType getNodeType() {
        return NodeType.BOOLEAN;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.BOOLEAN;
    }

    private void validateChild(RuleNode child) {
        if (child == null) {
            throw new IllegalArgumentException(Messages.sNullChild);
        }
        NodeOutputType childOutput = child.getOutputType();
        if (childOutput != NodeOutputType.BOOLEAN) {
            String message = String.format(Messages.sInvalidChildNodeOutputType,
                    getClass().getName(), NodeOutputType.BOOLEAN.name());
            throw new IllegalArgumentException(message);
        }
    }
}
