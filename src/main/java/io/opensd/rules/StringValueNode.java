package io.opensd.rules;

public class StringValueNode extends ValueNode {

    public StringValueNode(String value) {
        super(Type.STRING, value);
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.STRING;
    }
}
