package io.opensd.rules;

public class IntValueNode extends ValueNode {

    private long mValue;

    public IntValueNode(String value) {
        super(Type.INTEGER, value);
        try {
            mValue = Long.parseLong(value);
        } catch (NumberFormatException ex) {
            // throw an illegal argument to keep it consistent with other nodes
            String message = String.format(Messages.sIntParseError, value);
            throw new IllegalArgumentException(message);
        }
    }

    public long getIntValue() {
        return mValue;
    }

    public NodeOutputType getOutputType() {
        return NodeOutputType.NUMBER;
    }
}
