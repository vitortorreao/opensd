# OpenSD

OpenSD is an open source (and free) implementation of Subgroup Discovery 
solutions. We call "solutions" the many algorithm and heuristics designed to
solve a subgroup discovery problem. The objective of this project is to
eventually have all published solutions available to the public in an open 
source, free, well written and documented code.

There is no official language to the project, so we use Google's 
[bazel](https://bazel.build/) build tool, which is designed for projects with
multiple languages. That way, the only restriction to a programming language 
is that it must be officially supported by bazel.

Every developer, researcher and hobbyist is welcome to contribute to the 
project. We accept any sort of contribution, be it documentation, code 
efficiency improvement, bug fix or new implementation. Bear in mind, however,
that we only accept published solutions to Subgroup Discovery problems.
